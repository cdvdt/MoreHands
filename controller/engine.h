/*
 *  engine.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENGINE_H
#define ENGINE_H

#include "../model/setlist.h"
#include <unordered_map>
#include <vector>
#include <list>
#include <iostream>
#include "../model/alias.h"
#include "../model/events/internal/PlayElementEvent.h"

class Engine;

#include "abstractmidiclient.h"

#ifndef SET_MIDI_ENGINE
#define USE_ALSA
#endif

#ifdef USE_ALSA
#include "alsamidiclient.h"
#endif

#ifndef SET_THREAD_MECHANISM
#define USE_PTHREADS
#endif

#ifdef USE_PTHREADS
#include <pthread.h>
#include <semaphore.h>
#endif

enum MidiEngine {
    ALSA
};

enum ThreadMechanism {
    PTHREADS
};

struct Node {
    int music;
    int section;
};

/**
 * Engine for runtime side of MoreHands execution.
 */
class Engine
{
public:
    Engine(MidiEngine midiEngine = ALSA, ThreadMechanism threadMechanism = PTHREADS);
    bool exec();
    bool sendChord(int *chord);
    bool sendControlChanges(int *controlChanges);
    bool sendEvent(Alias alias);
    bool executeNext();
    bool isMidiThreadRunning();

    void setSetlist(Setlist *setlist);

    MoreHandsEvent *getMapping(MoreHandsEvent *event);

    bool keepThreadAlive();
    void consumePollRequest();
    void addRequestToPoll(MoreHandsEvent *event);
    void requestExitThreads();

private:
    void populateShortcutList();
    MidiEngine midiEngine;
    ThreadMechanism threadMechanism;

    Setlist *setlist;
    std::unordered_multimap<Alias, Node> shortcuts;
    std::unordered_multimap<MoreHandsEvent*, MoreHandsEvent*> eventMap;

    std::vector<AbstractElement *> requestQueue;

    int currentMusic;
    int currentSection;
    int currentElement;

    bool threadRunning;
    bool bKeepThreadAlive;

    AbstractMidiClient *client;

    std::list<MoreHandsEvent *> requestPoll;

    pthread_mutex_t threadRunningMutex;
    pthread_mutex_t requestPollMutex;

    sem_t requestPollSemaphore;
};

#endif // ENGINE_H
