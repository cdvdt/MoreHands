/*
 *  DesignEnvironment.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2018 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTROLLER_DESIGNENVIROMENT_H_
#define CONTROLLER_DESIGNENVIROMENT_H_

#include <unordered_map>
#include "../model/chord.h"
#include "../model/controlchanges.h"
#include "../model/fragment.h"
#include "../model/music.h"
#include "../model/section.h"
#include "../model/setlist.h"

// TODO serialization
/**
 * Holds data used in design time by the user.
 */
class DesignEnviroment {
public:
	/**
	 * Enumerates the types handled by this class
	 */
	enum ElementType {
		CHORD, CONTROL_CHANGES, FRAGMENT, MUSIC, SECTION, SETLIST
	};
	DesignEnviroment();
	virtual ~DesignEnviroment();
	/**
	 * Returns if a name is valid for use or not
	 * \param name name to be validated
	 * \param type type of element
	 * \return true if the name is not used and the type is valid, false instead
	 */
	bool isValidName(std::string name, ElementType type);

	//chord
	bool add(Chord &chord);
	void update(Chord &chord);
	bool remove(Chord &chord);

	//controlchanges
	bool add(ControlChanges &controlChanges);
	void update(ControlChanges &controlChanges);
	bool remove(ControlChanges &controlChanges);

	//fragment
	bool add(Fragment &fragment);
	void update(Fragment &fragment);
	bool remove(Fragment &fragment);

	//music
	bool add(Music &music);
	void update(Music &music);
	bool remove(Music &music);

	//section
	bool add(Section &section);
	void update(Section &section);
	bool remove(Section &section);

	//setlist
	bool add(std::string name, Setlist &setlist);
	void update(std::string name, Setlist &setlist);

	bool remove(std::string name, ElementType type);

private:
	std::unordered_map<std::string, Chord> chordLibrary;
	std::unordered_map<std::string, ControlChanges> controlChangesLibrary;
	std::unordered_map<std::string, Fragment> fragmentLibrary;
	std::unordered_map<std::string, Music> musicLibrary;
	std::unordered_map<std::string, Section> sectionLibrary;
	std::unordered_map<std::string, Setlist> setlistLibrary;

};

#endif /* CONTROLLER_DESIGNENVIROMENT_H_ */
