/*
 *  alsamidiclient.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALSAMIDICLIENT_H
#define ALSAMIDICLIENT_H

#include "abstractmidiclient.h"
#include <vector>
#include <string>

/*Alsa related stuff*/
#include <stdlib.h>
#include <unistd.h>
#include <alsa/asoundlib.h>

#ifndef SET_THREAD_MECHANISM
#define USE_PTHREADS
#endif

#ifdef USE_PTHREADS
#include <pthread.h>
#endif

#include "../model/events/midiSide/DefaultMidiEvent.h"
#include "../model/events/midiSide/ResultMidiEvent.h"
#include "../model/events/midiSide/NoteMidiEvent.h"
#include "../model/events/midiSide/ControlMidiEvent.h"
#include "../model/events/midiSide/QueueControlMidiEvent.h"
#include "../model/events/midiSide/ConnectMidiEvent.h"
#include "../model/events/midiSide/ExtMidiEvent.h"

/**
 * @brief The AlsaMidiClient class Implements an AbstractMidiClient based on alsa midi sequencer
 */
class AlsaMidiClient: public AbstractMidiClient {
public:
	AlsaMidiClient();

	/**
	 * @brief setupSequencer Default setup. Defines one in port and one out port, and MoreHands as name for the sequencer.
	 */
	virtual void setupSequencer();
	/**
	 * @brief setupSequencer Defines a configuration to this object.
	 * @param numInputs Number of in ports to be opened
	 * @param numOutputs Number of out ports to be opened
	 * @param sequencerName Name of this midi sequencer
	 */
	void setupSequencer(int numInputs, int numOutputs,
			std::string sequencerName);

	/**
	 * @brief openSequencer Effectively creates an alsa midi sequencer and open in and out ports.
	 * @return true if succeded, false otherwise
	 */
	virtual bool openSequencer();
	/**
	 * @brief creates the midi thread
	 * @return true if succeded, false otherwise
	 */
	virtual bool run();
	/**
	 * @brief Returns the opened in ports
	 * @return opened in ports
	 */
	const std::vector<int>& getInPorts() const {
		return inPorts;
	}
	/**
	 * @brief Returns the name used to open the ports
	 * @return name used to open the ports
	 */
	const std::string& getName() const {
		return name;
	}
	/**
	 * @brief Define the name used in the port names
	 */
	void setName(const std::string& name) {
		this->name = name;
	}
	/**
	 * @brief Returns the opened out ports
	 * @return opened out ports
	 */
	const std::vector<int>& getOutPorts() const {
		return outPorts;
	}
	/**
	 * @brief Returns the created sequencer
	 * @return the created sequencer
	 */
	snd_seq_t* getSequencer() const {
		return sequencer;
	}

	virtual void executeElement(AbstractElement *element);

	DefaultMidiEvent* makeEventFromAlsaSequencerEvent(snd_seq_event_t* ev);

private:
	std::string name;
	std::vector<int> inPorts;
	std::vector<int> outPorts;
	snd_seq_t *sequencer;
};

#endif // ALSAMIDICLIENT_H
