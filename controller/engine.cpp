/*
 *  engine.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "engine.h"

Engine::Engine(MidiEngine midiEngine, ThreadMechanism threadMechanism) :
		midiEngine(midiEngine), threadMechanism(threadMechanism), threadRunning(
				false) {
	if (midiEngine == ALSA) {
#ifdef USE_ALSA
		client = new AlsaMidiClient();
		client->setupSequencer();
		client->openSequencer();
#endif
	}
	if (threadMechanism == PTHREADS) {
#ifdef USE_PTHREADS
		threadRunningMutex = PTHREAD_MUTEX_INITIALIZER;
		requestPollMutex = PTHREAD_MUTEX_INITIALIZER;
		sem_init(&requestPollSemaphore, 0, 0);
#endif
	}
	client->setOwner(this);
}

bool Engine::isMidiThreadRunning() {
	return threadRunning;
}

bool Engine::keepThreadAlive() {
	pthread_mutex_lock(&threadRunningMutex);

	bool keep = bKeepThreadAlive;

	pthread_mutex_unlock(&threadRunningMutex);

	return keep;
}

void Engine::consumePollRequest() {
	sem_wait(&requestPollSemaphore);

	pthread_mutex_lock(&requestPollMutex);

	MoreHandsEvent *event = requestPoll.front();
	requestPoll.pop_front();

	pthread_mutex_unlock(&requestPollMutex);

	MoreHandsEvent *finalEvent = getMapping(event);

	Alias alias(*finalEvent);
	PlayElementEvent playElementEvent;

	if (finalEvent == &playElementEvent) executeNext();
	else sendEvent(alias);
	delete finalEvent;
	delete event;
}

void Engine::addRequestToPoll(MoreHandsEvent* event) {
	pthread_mutex_lock(&requestPollMutex);

	requestPoll.push_back(event);

	pthread_mutex_unlock(&requestPollMutex);

	sem_post(&requestPollSemaphore);
}

bool Engine::sendChord(int* chord) {
}

bool Engine::sendControlChanges(int* controlChanges) {
}

void Engine::setSetlist(Setlist* setlist) {
	this->setlist = setlist;
	currentElement = 0;
	currentMusic = 0;
	currentSection = 0;
	populateShortcutList();
}

void Engine::requestExitThreads() {
	pthread_mutex_lock(&threadRunningMutex);

	bKeepThreadAlive = false;

	pthread_mutex_unlock(&threadRunningMutex);
}

void Engine::populateShortcutList() {
	Music** musics = setlist->getSetlist();
	int musicsSize = setlist->size();
	for (int i = 0; i < musicsSize; i++) {
		Section** sections = musics[i]->getSections();
		int sectionsSize = musics[i]->size();
		Node node = { i, 0 };
		shortcuts.emplace(musics[i]->getAlias(), node);

		for (int j = 0; j < sectionsSize; i++) {
			Node node = { i, j };
			shortcuts.emplace(sections[i]->getAlias(), node);
		}
	}
}

bool Engine::sendEvent(Alias alias) {
	/*std::unordered_map<Alias, Node>::iterator it = shortcuts.find(alias);
	 if (it != shortcuts.end()) {
	 for ()
	 currentMusic = it->second.music;
	 currentSection = it->second.section;
	 return true;
	 } else {
	 return false;
	 }*/
	auto range = shortcuts.equal_range(alias);
	if (range.first != shortcuts.end()) {
		currentMusic = range.first->second.music;
		currentSection = range.first->second.section;
	} else {
		return false;
	}
	for (auto it = range.first; it != range.second; it++) {
		if (it->second.music == currentMusic) {
			currentMusic = it->second.music;
			currentSection = it->second.section;
			return true;
		}
	}
	return true;
}

/*bool Engine::executeNext() {
 Music *currentMusic = setlist->getCurrent();

 if (currentMusic != nullptr) {
 AbstractElement *currentElement = currentMusic->getCurrent();

 if (currentElement != nullptr) {
 requestQueue.push_back(currentElement);
 setlist->stepForward();
 return true;
 } else {
 return false;
 }
 } else {
 return false;
 }
 }*/

bool Engine::executeNext() {
	if (currentMusic < (*setlist).size()) {
		if (currentSection < (*setlist)[currentMusic]->size()) {
			Music* current = (*setlist)[currentMusic];
			if (currentElement < (*current)[currentSection]->size()) {
				requestQueue.push_back(
						(*(*current)[currentSection])[currentElement]);
				currentElement++;
				return true;
			} else {
				currentElement = 0;
				currentSection++;
				return executeNext();
			}
		} else {
			currentSection = 0;
			currentElement = 0;
			currentMusic++;
			return executeNext();
		}
	} else {
		return false;
	}
}

#ifdef USE_PTHREADS
void *enginePthread(void *owner) {
	Engine *engine = (Engine *) owner;

	while (engine->keepThreadAlive()) {
		engine->consumePollRequest();
	}
	pthread_exit(NULL);
}
#endif

bool Engine::exec() {
	threadRunning = true;
	if (client->run() == false)
		threadRunning = false;
	if (threadMechanism == PTHREADS) {
#ifdef USE_PTHREADS
		std::cerr << "trying to create engine thread..." << std::endl;
		pthread_t engineThread;
		int rc = pthread_create(&engineThread, NULL, enginePthread,
				(void *) this);
		if (rc) {
			std::cerr << "ERROR; return code from pthread_create() is " + rc
					<< std::endl;
			threadRunning = false;
		}
#else
		return false;
#endif
	}

	bKeepThreadAlive = threadRunning;
	return threadRunning;
}

MoreHandsEvent* Engine::getMapping(MoreHandsEvent* event) {
	auto range = eventMap.equal_range(event);
	return (range.first != eventMap.end() ? range.first->second : event);
}
