/*
 *  DesignEnvironment.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2018 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DesignEnviroment.h"

DesignEnviroment::DesignEnviroment() {
	// TODO Auto-generated constructor stub

}

DesignEnviroment::~DesignEnviroment() {
	// TODO Auto-generated destructor stub
}

bool DesignEnviroment::isValidName(std::string name, ElementType type) {
	switch (type) {
	case CHORD: return chordLibrary.count(name) == 0;
		break;
	case CONTROL_CHANGES: return controlChangesLibrary.count(name) == 0;
			break;
	case FRAGMENT: return fragmentLibrary.count(name) == 0;
			break;
	case MUSIC: return musicLibrary.count(name) == 0;
			break;
	case SECTION: return sectionLibrary.count(name) == 0;
			break;
	case SETLIST: return setlistLibrary.count(name) == 0;
			break;
	default: return false;
	}
}

bool DesignEnviroment::add(Chord& chord) {
	return chordLibrary.emplace(chord.name, chord).second;
}

void DesignEnviroment::update(Chord& chord) {

}

bool DesignEnviroment::add(ControlChanges& controlChanges) {
	return controlChangesLibrary.emplace(controlChanges.name, controlChanges).second;
}

void DesignEnviroment::update(ControlChanges& controlChanges) {
}

bool DesignEnviroment::add(Fragment& fragment) {
	return fragmentLibrary.emplace(fragment.name, fragment).second;
}

void DesignEnviroment::update(Fragment& fragment) {
}

bool DesignEnviroment::add(Music& music) {
	return musicLibrary.emplace(music.name, music).second;
}

void DesignEnviroment::update(Music& music) {
}

bool DesignEnviroment::add(Section& section) {

}

void DesignEnviroment::update(Section& section) {
}

bool DesignEnviroment::remove(Chord& chord) {
	return chordLibrary.erase(chord.name) == 1;
}

bool DesignEnviroment::remove(ControlChanges &controlChanges) {
	return controlChangesLibrary.erase(controlChanges.name) == 1;
}

bool DesignEnviroment::remove(Fragment& fragment) {
	return fragmentLibrary.erase(fragment.name) == 1;
}

bool DesignEnviroment::remove(Music& music) {
	return musicLibrary.erase(music.name) == 1;
}

bool DesignEnviroment::remove(Section& section) {
	return sectionLibrary.erase(section.name) == 1;
}

bool DesignEnviroment::add(std::string name, Setlist& setlist) {
	return sectionLibrary.emplace(name, setlist).second;
}

void DesignEnviroment::update(std::string name, Setlist& setlist) {
}

bool DesignEnviroment::remove(std::string name, ElementType type) {
	switch (type) {
	case CHORD: return chordLibrary.erase(name) == 1;
		break;
	case CONTROL_CHANGES: return controlChangesLibrary.erase(name) == 1;
			break;
	case FRAGMENT: return fragmentLibrary.erase(name) == 1;
			break;
	case MUSIC: return musicLibrary.erase(name) == 1;
			break;
	case SECTION: return sectionLibrary.erase(name) == 1;
			break;
	case SETLIST: return setlistLibrary.erase(name) == 1;
			break;
	default: return false;
	}
}
