/*
 *  abstractmidiclient.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABSTRACTMIDICLIENT_H
#define ABSTRACTMIDICLIENT_H

class AbstractMidiClient;

//#include "engine.h"
class Engine;

#include "../model/abstractelement.h"

class AbstractMidiClient {
public:
	AbstractMidiClient();

	virtual ~AbstractMidiClient();

	/**
	 * @brief setupSequencer Defines the sequencer setup. Must hold default values. May be overloaded.
	 */
	virtual void setupSequencer() = 0;
	/**
	 * @brief openSequencer Creates a sequencer and open ports.
	 * @return true if succeded, false otherwise.
	 */
	virtual bool openSequencer() = 0;
	/**
	 * \brief Sets the owner for this client.
	 * \param owner Owner for this client
	 */
	virtual void setOwner(Engine *owner);
	virtual void executeElement(AbstractElement *element) = 0;
	virtual Engine *getOwner();
	virtual bool run() = 0;

private:
	Engine *owner;
};

#endif // ABSTRACTMIDICLIENT_H
