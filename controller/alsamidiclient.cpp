/*
 *  alsamidiclient.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "alsamidiclient.h"
#include "engine.h"

//for debugging
#ifndef QT_NO_DEBUG
#include <iostream>
#endif

AlsaMidiClient::AlsaMidiClient() {

}

void AlsaMidiClient::setupSequencer(int numInputs, int numOutputs,
		std::string sequencerName) {
	name = sequencerName;
	inPorts.resize(numInputs);
	outPorts.resize(numOutputs);
#ifndef QT_NO_DEBUG
	std::cout << name << std::endl;
#endif
}

void AlsaMidiClient::setupSequencer() {
	setupSequencer(1, 1, "MoreHands");
}

bool AlsaMidiClient::openSequencer() {
	if (snd_seq_open(&sequencer, "default", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
		fprintf(stderr, "Error opening ALSA sequencer.\n");
		return false;
	}
	snd_seq_set_client_name(sequencer, name.data());
	for (uint i = 0; i < inPorts.capacity(); i++) {
		std::string portname = name + " IN " + std::to_string(i);
		//sprintf(portname, "%s IN %d", seq_name, i);
		if ((inPorts[i] = snd_seq_create_simple_port(sequencer, portname.data(),
				SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE,
				SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
			fprintf(stderr, "Error creating sequencer port.\n");
			return false;
		}
	}
	for (uint i = 0; i < outPorts.capacity(); i++) {
		std::string portname = name + " OUT " + std::to_string(i);
		//sprintf(portname, "%s OUT %d", seq_name, i);
		if ((outPorts[i] = snd_seq_create_simple_port(sequencer,
				portname.data(),
				SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ,
				SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
			fprintf(stderr, "Error creating sequencer port.\n");
			return false;
		}
	}
#ifndef QT_NO_DEBUG
	std::cout << name << " in ports: " << inPorts.size() << " out ports: "
			<< outPorts.size() << std::endl;
#endif
	return true;
}

#ifdef USE_PTHREADS
void *audioPthread(void *owner) {
	AlsaMidiClient *client = (AlsaMidiClient *) owner;

	int npfd;
	struct pollfd *pfd;

	snd_seq_t *sequencer = client->getSequencer();

	npfd = snd_seq_poll_descriptors_count(sequencer, POLLIN);
	std::cout << "number of ports polled: " << npfd << std::endl;
	pfd = (struct pollfd *) alloca(npfd * sizeof(struct pollfd));
	snd_seq_poll_descriptors(sequencer, pfd, npfd, POLLIN);
	while (client->getOwner()->isMidiThreadRunning() == true) {
		if (poll(pfd, npfd, -1) > 0) {
			do {
				snd_seq_event_t *ev;
				snd_seq_event_input(sequencer, &ev);
				DefaultMidiEvent *event =
						client->makeEventFromAlsaSequencerEvent(ev);
				std::cout << "receiving message" << std::endl;
				std::cout << "input message received: " << event->to_string()
						<< std::endl;
				client->getOwner()->addRequestToPoll(event);
				/*std::cout << "receiving message" << std::endl;
				 snd_seq_event_t *ev;
				 snd_seq_event_input(sequencer, &ev);
				 std::cout << "input message received: " << (int) ev->type
				 << std::endl;
				 std::cout << "source: " << std::to_string(ev->source.client)
				 << ":" << std::to_string(ev->source.port) << std::endl;
				 std::cout << "dest: " << std::to_string(ev->dest.client) << ":"
				 << std::to_string(ev->dest.port) << std::endl;

				 if (ev->type == 10) {
				 std::cout << "channel: "
				 << std::to_string(ev->data.control.channel)
				 << std::endl;
				 std::cout << "param: " << ev->data.control.param
				 << std::endl;
				 std::cout << "value: " << ev->data.control.value
				 << std::endl;
				 }
				 snd_seq_free_event(ev);*/
			} while (snd_seq_event_input_pending(sequencer, 0) > 0);
		}
	}
	pthread_exit(NULL);
}
#endif

bool AlsaMidiClient::run() {
#ifdef USE_PTHREADS
	std::cerr << "trying to create midi thread..." << std::endl;
	pthread_t audioThread;
	int rc = pthread_create(&audioThread, NULL, audioPthread, (void *) this);
	if (rc) {
		std::cerr << "ERROR; return code from pthread_create() is " + rc
				<< std::endl;
		return false;
	}
	return true;
#else
	return false;
#endif
}

DefaultMidiEvent* AlsaMidiClient::makeEventFromAlsaSequencerEvent(
		snd_seq_event_t* ev) {
	switch (ev->type) {
	case SND_SEQ_EVENT_SYSTEM:
	case SND_SEQ_EVENT_RESULT:
		return new ResultMidiEvent();
	case SND_SEQ_EVENT_NOTE:
	case SND_SEQ_EVENT_NOTEON:
	case SND_SEQ_EVENT_NOTEOFF:
	case SND_SEQ_EVENT_KEYPRESS: {
		NoteMidiEvent *note = new NoteMidiEvent(ev->type, ev->flags, ev->tag,
				ev->queue, ev->data.note.channel, ev->data.note.note,
				ev->data.note.velocity, ev->data.note.off_velocity,
				ev->data.note.duration, NoteMidiEvent::NOTE, {
						ev->source.client, ev->source.port }, { ev->dest.client,
						ev->dest.port });
		switch (ev->type) {
		case SND_SEQ_EVENT_NOTE:
			note->setNoteType(NoteMidiEvent::NOTE);
			break;
		case SND_SEQ_EVENT_NOTEON:
			note->setNoteType(NoteMidiEvent::NOTE_ON);
			break;
		case SND_SEQ_EVENT_NOTEOFF:
			note->setNoteType(NoteMidiEvent::NOTE_OFF);
			break;
		case SND_SEQ_EVENT_KEYPRESS:
			note->setNoteType(NoteMidiEvent::KEY_PRESSURE);
			break;
		default:
			break;
		}
		return note;
	}
	case SND_SEQ_EVENT_CONTROLLER:
	case SND_SEQ_EVENT_PGMCHANGE:
	case SND_SEQ_EVENT_CHANPRESS:
	case SND_SEQ_EVENT_PITCHBEND:
	case SND_SEQ_EVENT_CONTROL14:
	case SND_SEQ_EVENT_NONREGPARAM:
	case SND_SEQ_EVENT_REGPARAM:
	case SND_SEQ_EVENT_SONGPOS:
	case SND_SEQ_EVENT_SONGSEL:
	case SND_SEQ_EVENT_QFRAME:
	case SND_SEQ_EVENT_TIMESIGN:
	case SND_SEQ_EVENT_KEYSIGN:
		return new ControlMidiEvent(ev->type, ev->flags, ev->tag, ev->queue,
				ev->data.control.channel, ev->data.control.param,
				ev->data.control.value, { ev->source.client, ev->source.port },
				{ ev->dest.client, ev->dest.port });
	case SND_SEQ_EVENT_START:
	case SND_SEQ_EVENT_CONTINUE:
	case SND_SEQ_EVENT_STOP:
	case SND_SEQ_EVENT_SETPOS_TICK:
	case SND_SEQ_EVENT_SETPOS_TIME:
	case SND_SEQ_EVENT_TEMPO:
	case SND_SEQ_EVENT_CLOCK:
	case SND_SEQ_EVENT_TICK:
	case SND_SEQ_EVENT_QUEUE_SKEW:
	case SND_SEQ_EVENT_SYNC_POS:
		return new QueueControlMidiEvent();
	case SND_SEQ_EVENT_TUNE_REQUEST:
	case SND_SEQ_EVENT_RESET:
	case SND_SEQ_EVENT_SENSING:
	case SND_SEQ_EVENT_ECHO:
	case SND_SEQ_EVENT_OSS:
		return new DefaultMidiEvent();
	case SND_SEQ_EVENT_CLIENT_START:
	case SND_SEQ_EVENT_CLIENT_EXIT:
	case SND_SEQ_EVENT_CLIENT_CHANGE:
	case SND_SEQ_EVENT_PORT_START:
	case SND_SEQ_EVENT_PORT_EXIT:
	case SND_SEQ_EVENT_PORT_CHANGE:
		return new DefaultMidiEvent();
	case SND_SEQ_EVENT_PORT_SUBSCRIBED:
	case SND_SEQ_EVENT_PORT_UNSUBSCRIBED:
		return new ConnectMidiEvent();
	case SND_SEQ_EVENT_USR0:
	case SND_SEQ_EVENT_USR1:
	case SND_SEQ_EVENT_USR2:
	case SND_SEQ_EVENT_USR3:
	case SND_SEQ_EVENT_USR4:
	case SND_SEQ_EVENT_USR5:
	case SND_SEQ_EVENT_USR6:
	case SND_SEQ_EVENT_USR7:
	case SND_SEQ_EVENT_USR8:
	case SND_SEQ_EVENT_USR9:
		return new DefaultMidiEvent();
	case SND_SEQ_EVENT_SYSEX:
	case SND_SEQ_EVENT_BOUNCE:
	case SND_SEQ_EVENT_USR_VAR0:
	case SND_SEQ_EVENT_USR_VAR1:
	case SND_SEQ_EVENT_USR_VAR2:
	case SND_SEQ_EVENT_USR_VAR3:
	case SND_SEQ_EVENT_USR_VAR4:
		return new ExtMidiEvent();
	case SND_SEQ_EVENT_NONE:
		return nullptr;
	}
}

void AlsaMidiClient::executeElement(AbstractElement* element) {
}
