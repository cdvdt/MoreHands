Aims to help musicians who want to control more than one equipment without relying on click tracks.
Send midi signals anywhere with a click of a button, or key or pedal or anything else.
It also allow you to jump from musics or sections with a click.
Currently uses Qt for ui, alsa for interfacing midi devices and libxml2 for reading xml files, but multi-platform and jack support is planned
