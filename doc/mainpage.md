\mainpage MoreHands
Play everything else, we take care of this for you
\section about About MoreHands

Aims to help musicians who want to control more than one equipment without relying on click tracks.

Send MIDI signals anywhere with a click of a button, or key or pedal or wherever you want.

It also allow you to jump from musics or sections with a click.

Currently uses Qt for ui, alsa for interfacing midi devices and libxml2 for reading xml files, but multi-platform and jack support is planned

