var dir_ab068440026ba763b6e4febe8f84aa05 =
[
    [ "internal", "dir_753fc3f9e385b48eda9bcbc1c5d5a39a.html", "dir_753fc3f9e385b48eda9bcbc1c5d5a39a" ],
    [ "midiSide", "dir_d01c76386b6f3d6f47ed10479bf8713e.html", "dir_d01c76386b6f3d6f47ed10479bf8713e" ],
    [ "systemSide", "dir_8aec6328a84fb9492436777e698df74c.html", "dir_8aec6328a84fb9492436777e698df74c" ],
    [ "ConnectMidiEvent.d", "_connect_midi_event_8d_source.html", null ],
    [ "ControlMidiEvent.d", "_control_midi_event_8d_source.html", null ],
    [ "DefaultMidiEvent.d", "_default_midi_event_8d_source.html", null ],
    [ "ExtMidiEvent.d", "_ext_midi_event_8d_source.html", null ],
    [ "MoreHandsEvent.d", "_more_hands_event_8d_source.html", null ],
    [ "NoteMidiEvent.d", "_note_midi_event_8d_source.html", null ],
    [ "QueueControlMidiEvent.d", "_queue_control_midi_event_8d_source.html", null ],
    [ "ResultMidiEvent.d", "_result_midi_event_8d_source.html", null ]
];