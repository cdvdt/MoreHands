var struct_command_request =
[
    [ "CommandRequest", "struct_command_request.html#a5d91354a8695e447f1ddda7151604e2c", null ],
    [ "toString", "struct_command_request.html#a2b0429eba98d9e3f60b64bd11167d0e5", null ],
    [ "data", "struct_command_request.html#a0c2b66ab23ce4f449f3f775e905ccb24", null ],
    [ "dataType", "struct_command_request.html#ae54213049289bba7420e5b29192ee463", null ],
    [ "isOpen", "struct_command_request.html#ac5c9e409f6330b7a1efb5146b6c2d6e1", null ],
    [ "type", "struct_command_request.html#a5c9b62bc7a90c37a92b4642b14a01e51", null ]
];