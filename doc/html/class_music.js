var class_music =
[
    [ "Music", "class_music.html#ab5143f67c021bc77894c8e91de2b916b", null ],
    [ "addSection", "class_music.html#aad14af3d9a1e8edc1624899b452ea0ce", null ],
    [ "getAlias", "class_music.html#a1668e4bbfdbee90fda63d87a99027edf", null ],
    [ "getContent", "class_music.html#acdc648fe0b9c6a3cbaeaff2395f476a8", null ],
    [ "getCurrent", "class_music.html#afff546864866d73723aa791992a0c4be", null ],
    [ "getName", "class_music.html#ae470a481d8df9352e720613544702803", null ],
    [ "getSectionAt", "class_music.html#ab9c56f16d1712a01e9d14cca09db7f0a", null ],
    [ "getSections", "class_music.html#a309ca7a62811923e98d1565f62640a68", null ],
    [ "getSectionsSize", "class_music.html#adf58f1a1dd702a2b1db588f41b2f0125", null ],
    [ "operator[]", "class_music.html#a8aeda71cdd368e51c58514f148f78c3f", null ],
    [ "removeSection", "class_music.html#aa64131993a0bdb17f471c64a0c3caf38", null ],
    [ "setAlias", "class_music.html#ab9e5338537326b90fcbd082984d9db2b", null ],
    [ "setName", "class_music.html#aca99fab8b10d48706a34a42844b1aa3e", null ],
    [ "size", "class_music.html#a6a7246841ea904a679121ca1a7c5360c", null ],
    [ "startSection", "class_music.html#a4abde9fd33af734242d86ec729a4f669", null ],
    [ "stepForward", "class_music.html#a40e63a1974d556f84a4cfcf46d99d215", null ],
    [ "alias", "class_music.html#a7e74dbcbb2acc7730085d7a7d781f169", null ],
    [ "name", "class_music.html#a66315b0034901a7d0a2c28dfc062ab9d", null ]
];