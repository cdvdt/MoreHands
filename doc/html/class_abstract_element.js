var class_abstract_element =
[
    [ "AbstractElement", "class_abstract_element.html#a3c4cbd44f231499e865237e9841aaf0d", null ],
    [ "getContent", "class_abstract_element.html#a2709dac69ed1e85f3648ff40e36eb323", null ],
    [ "getName", "class_abstract_element.html#a52bc7f421a1399c858dd37d2af72a7bf", null ],
    [ "getSurname", "class_abstract_element.html#a8aee3eb100d10a95c84896a2e258eb06", null ],
    [ "setName", "class_abstract_element.html#a3d9df8c44c0ee0d8dde2f88300e8b90a", null ],
    [ "setSurname", "class_abstract_element.html#a0b32d8ad629c9d589abe24f9860f09ee", null ],
    [ "name", "class_abstract_element.html#a1450084a2f3860a5ca001dcd7927563a", null ],
    [ "surname", "class_abstract_element.html#abccdd5b97732319c2833b05cc3d4cab3", null ]
];