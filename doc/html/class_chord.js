var class_chord =
[
    [ "Chord", "class_chord.html#ad587105fd399d940384c2ede87c7defd", null ],
    [ "Chord", "class_chord.html#ac7f0357d126baca11855846bcb865770", null ],
    [ "Chord", "class_chord.html#ac87d1581863432c54609a339c1aa867f", null ],
    [ "getChordNotes", "class_chord.html#add40bd703a175d9fe20af86d26fb453b", null ],
    [ "getContent", "class_chord.html#a4cc27bf5eadcb2b1b27cce7af5b3148f", null ],
    [ "getName", "class_chord.html#a52bc7f421a1399c858dd37d2af72a7bf", null ],
    [ "getNotesAsString", "class_chord.html#a10d6b9c60c629e9d4d176fd641d53fed", null ],
    [ "getSurname", "class_chord.html#a8aee3eb100d10a95c84896a2e258eb06", null ],
    [ "setChordNotes", "class_chord.html#ac750fac42902a766cc61644d554a8115", null ],
    [ "setName", "class_chord.html#a3d9df8c44c0ee0d8dde2f88300e8b90a", null ],
    [ "setSurname", "class_chord.html#a0b32d8ad629c9d589abe24f9860f09ee", null ],
    [ "name", "class_chord.html#a1450084a2f3860a5ca001dcd7927563a", null ],
    [ "surname", "class_chord.html#abccdd5b97732319c2833b05cc3d4cab3", null ]
];