var class_abstract_section =
[
    [ "AbstractSection", "class_abstract_section.html#ab42c16c0b07bfba95e13c9b9391837d8", null ],
    [ "getAlias", "class_abstract_section.html#a1668e4bbfdbee90fda63d87a99027edf", null ],
    [ "getContent", "class_abstract_section.html#acdc648fe0b9c6a3cbaeaff2395f476a8", null ],
    [ "getCurrent", "class_abstract_section.html#a276c8e27b9b49e96b87f0cbf01100439", null ],
    [ "getName", "class_abstract_section.html#ae470a481d8df9352e720613544702803", null ],
    [ "setAlias", "class_abstract_section.html#ab9e5338537326b90fcbd082984d9db2b", null ],
    [ "setName", "class_abstract_section.html#aca99fab8b10d48706a34a42844b1aa3e", null ],
    [ "size", "class_abstract_section.html#a9982b5da5c6ad1cd22b13e9f6c01f4c2", null ],
    [ "startSection", "class_abstract_section.html#a4abde9fd33af734242d86ec729a4f669", null ],
    [ "stepForward", "class_abstract_section.html#a77c245e6af95749230c8dedf1d867902", null ],
    [ "alias", "class_abstract_section.html#a7e74dbcbb2acc7730085d7a7d781f169", null ],
    [ "name", "class_abstract_section.html#a66315b0034901a7d0a2c28dfc062ab9d", null ]
];