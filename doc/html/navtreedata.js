var NAVTREE =
[
  [ "MoreHands", "index.html", [
    [ "About MoreHands", "index.html#about", null ],
    [ "CHANGELOG", "md__c_h_a_n_g_e_l_o_g.html", null ],
    [ "DESCRIPTION", "md__d_e_s_c_r_i_p_t_i_o_n.html", null ],
    [ "README", "md__r_e_a_d_m_e.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"class_ext_midi_event.html#a7eb752ca72620ebc182cd955985e5a56",
"class_text_ui_1_1location.html#a68d34b160b6dc73fef8be875ff0b172c",
"struct_text_ui_1_1_intern_parser_1_1token.html#a90b31e2a638e358aa411e90ccfb3e601ad474a8e5b1f6b372901ec26f2b6fb3f1"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';