var class_alsa_midi_client =
[
    [ "AlsaMidiClient", "class_alsa_midi_client.html#a6408b1fc145ed6b433da61bc61c52cfa", null ],
    [ "executeElement", "class_alsa_midi_client.html#abeaa14d031ffc19136ad5df75ab90ad2", null ],
    [ "getInPorts", "class_alsa_midi_client.html#a4471e0de975420b09ad358f2aa0e3f9c", null ],
    [ "getName", "class_alsa_midi_client.html#a08afc0192d0b6cf2ba2a0fef5b711999", null ],
    [ "getOutPorts", "class_alsa_midi_client.html#ab18b3f5eebef8473938daa20e11230ea", null ],
    [ "getOwner", "class_alsa_midi_client.html#aa00da55813cc0e00ea3e4eb1ed317142", null ],
    [ "getSequencer", "class_alsa_midi_client.html#a58a5fcf626b74ac2d90e48d98ed8cd09", null ],
    [ "makeEventFromAlsaSequencerEvent", "class_alsa_midi_client.html#a03da09d0b05990edd8dc2de70b7ff737", null ],
    [ "openSequencer", "class_alsa_midi_client.html#a1f461b6708d430657255b9707574782d", null ],
    [ "run", "class_alsa_midi_client.html#a8dda91756abf865cc8a1e833febf61ab", null ],
    [ "setName", "class_alsa_midi_client.html#a5e17b79166a82fddeeaea7483e1ec762", null ],
    [ "setOwner", "class_alsa_midi_client.html#a689040fefd1f8c73b4d5eccae9806c97", null ],
    [ "setupSequencer", "class_alsa_midi_client.html#a6546d22521ee633e5ce8be844700f10a", null ],
    [ "setupSequencer", "class_alsa_midi_client.html#a45e7f45e900e1cf2b31f56623ee796fe", null ]
];