var class_setlist =
[
    [ "Setlist", "class_setlist.html#aa10b22575ceeb3106fd6da8469bacd83", null ],
    [ "addMusic", "class_setlist.html#ae7a094c98225d7858b3ecbe4dab9ac9a", null ],
    [ "getContent", "class_setlist.html#a8fdb5a751564b2325fe92751da5b5631", null ],
    [ "getCurrent", "class_setlist.html#a4ba226802d0c1d4c715402f56065be80", null ],
    [ "getSetlist", "class_setlist.html#aef061157f4bc2fae0c1fd2ffdc848e73", null ],
    [ "getSetlistSize", "class_setlist.html#a73c4174ff1b50723c0b92926f4276c3c", null ],
    [ "operator[]", "class_setlist.html#a7dc9c422aa2b24860fd6715dcfd4109d", null ],
    [ "removeMusic", "class_setlist.html#acbe41a7b50aeafb660844a8a5e0c343e", null ],
    [ "size", "class_setlist.html#acffc834236a485aa34b36370931a5ab1", null ],
    [ "stepForward", "class_setlist.html#a8f3411febf1a45045ef04292d827c80b", null ],
    [ "updateCurrent", "class_setlist.html#a0fe448ee20b07a1591f2d742cf466670", null ]
];