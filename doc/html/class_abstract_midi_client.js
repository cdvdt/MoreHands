var class_abstract_midi_client =
[
    [ "AbstractMidiClient", "class_abstract_midi_client.html#a0f8342226435b9c5cad3b64b7cc5277b", null ],
    [ "~AbstractMidiClient", "class_abstract_midi_client.html#a598fbfbbcec3d8f255a3bfe59bd82128", null ],
    [ "executeElement", "class_abstract_midi_client.html#aa7654cf68b7a77c36e1a889728acf903", null ],
    [ "getOwner", "class_abstract_midi_client.html#aa00da55813cc0e00ea3e4eb1ed317142", null ],
    [ "openSequencer", "class_abstract_midi_client.html#a093be9a311d8f9dcf5f3cd475eafc78a", null ],
    [ "run", "class_abstract_midi_client.html#a5b2c475fdafc3bf1529d6cb09c346a5c", null ],
    [ "setOwner", "class_abstract_midi_client.html#a689040fefd1f8c73b4d5eccae9806c97", null ],
    [ "setupSequencer", "class_abstract_midi_client.html#afeceb68a7712db0835ca9335c3412c06", null ]
];