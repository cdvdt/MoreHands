var class_section =
[
    [ "Section", "class_section.html#a77b88e06692841ba49559d22a25a09f9", null ],
    [ "addElement", "class_section.html#a3f3fb83940c85919d52b5f0045f74c50", null ],
    [ "addElement", "class_section.html#a9addbbfc99d08dbd09f663bf39de89e3", null ],
    [ "getAlias", "class_section.html#a1668e4bbfdbee90fda63d87a99027edf", null ],
    [ "getContent", "class_section.html#a5726817c2c46fa9158dc0f96dc7410ea", null ],
    [ "getCurrent", "class_section.html#a831822e7adfa0b81c1f6850791d464f5", null ],
    [ "getElementAt", "class_section.html#a1e641b5ffdb7d6e8d781c2adbe2db32f", null ],
    [ "getName", "class_section.html#ae470a481d8df9352e720613544702803", null ],
    [ "operator[]", "class_section.html#abc23b4e15cefed8f6cbdeab41b7e97c4", null ],
    [ "setAlias", "class_section.html#ab9e5338537326b90fcbd082984d9db2b", null ],
    [ "setName", "class_section.html#aca99fab8b10d48706a34a42844b1aa3e", null ],
    [ "size", "class_section.html#a1ea675fa957ee6b52465bb4dfb39e210", null ],
    [ "startSection", "class_section.html#af95cc03851aa58bec490808d238f328c", null ],
    [ "stepForward", "class_section.html#aa5805b71acc1297fd10d0af3da661ac1", null ],
    [ "alias", "class_section.html#a7e74dbcbb2acc7730085d7a7d781f169", null ],
    [ "name", "class_section.html#a66315b0034901a7d0a2c28dfc062ab9d", null ]
];