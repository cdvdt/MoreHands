/*
 * TextUiParser.cpp
 *
 *  Created on: 07/07/2017
 *      Author: cdvdt
 */

#include "TextUiParser.h"

namespace TextUi {

TextUiParser::TextUiParser() {
	lexer = new Lexer(&std::cin);
	helper = new InternalParserHelper();
	parser = new InternParser((*lexer), (*helper));
}

TextUiParser::TextUiParser(std::stringstream &stream) {
	lexer = new Lexer(&stream);
	helper = new InternalParserHelper();
	parser = new InternParser((*lexer), (*helper));
}

TextUiParser::~TextUiParser() {
	delete lexer;
	delete parser;
	delete helper;
	// TODO Auto-generated destructor stub
}

int TextUiParser::parse(std::vector<CommandRequest *>& commands) {
	//this->commands = &commands;
	helper->setCommandList(commands);
	int parseResult = parser->parse();
	if (parseResult != 0) commands.pop_back();

	return parseResult;
}

} /* namespace TextUi */
