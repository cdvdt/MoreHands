// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.

#line 37 "TextUiParser.tab.cpp" // lalr1.cc:404

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "TextUiParser.tab.hpp"

// User implementation prologue.

#line 51 "TextUiParser.tab.cpp" // lalr1.cc:412
// Unqualified %code blocks.
#line 53 "TextUiParser.y" // lalr1.cc:413

   #include <iostream>
   #include <cstdlib>
   #include <fstream>
   #include "CommandRequest.h"
   #include "TextUiLexer.h"
   
   /* include for all driver functions */
   //#include "mc_driver.hpp"

void addNote(std::string note) {
	std::cout << "a note " << note << std::endl;
}

void addName(std::string name) {
	std::cout << "a name " << name << std::endl;
}

void createChord () {
	std::cout << "a chord " << std::endl;
}

void createSection () {
	std::cout << "a section " << std::endl;
}

void createMusic () {
	std::cout << "a music " << std::endl;
}

void createSetlist () {
	std::cout << "a setlist " << std::endl;
}

void start () {
	std::cout << "start!" << std::endl;
}

void list () {
	std::cout << "list!" << std::endl;
}

void add () {
	std::cout << "adicionando " << std::endl;
}

void remove () {
	std::cout << "removendo " << std::endl;
}

void destroy () {
	std::cout << "destroi " << std::endl;
}

void quit() {
	exit(0);
}

#undef yylex
#define yylex lexer.yylex

#line 115 "TextUiParser.tab.cpp" // lalr1.cc:413


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

#line 4 "TextUiParser.y" // lalr1.cc:479
namespace TextUi {
#line 201 "TextUiParser.tab.cpp" // lalr1.cc:479

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  InternParser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              // Fall through.
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }


  /// Build a parser object.
  InternParser::InternParser (TextUi::Lexer &lexer_yyarg, TextUi::InternalParserHelper &helper_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      lexer (lexer_yyarg),
      helper (helper_yyarg)
  {}

  InternParser::~InternParser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/

  inline
  InternParser::syntax_error::syntax_error (const location_type& l, const std::string& m)
    : std::runtime_error (m)
    , location (l)
  {}

  // basic_symbol.
  template <typename Base>
  inline
  InternParser::basic_symbol<Base>::basic_symbol ()
    : value ()
  {}

  template <typename Base>
  inline
  InternParser::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value ()
    , location (other.location)
  {
      switch (other.type_get ())
    {
      case 18: // NAME
      case 19: // NOTE
        value.copy< std::string > (other.value);
        break;

      default:
        break;
    }

  }


  template <typename Base>
  inline
  InternParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {
    (void) v;
      switch (this->type_get ())
    {
      case 18: // NAME
      case 19: // NOTE
        value.copy< std::string > (v);
        break;

      default:
        break;
    }
}


  // Implementation of basic_symbol constructor for each type.

  template <typename Base>
  InternParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {}

  template <typename Base>
  InternParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}


  template <typename Base>
  inline
  InternParser::basic_symbol<Base>::~basic_symbol ()
  {
    clear ();
  }

  template <typename Base>
  inline
  void
  InternParser::basic_symbol<Base>::clear ()
  {
    // User destructor.
    symbol_number_type yytype = this->type_get ();
    basic_symbol<Base>& yysym = *this;
    (void) yysym;
    switch (yytype)
    {
   default:
      break;
    }

    // Type destructor.
    switch (yytype)
    {
      case 18: // NAME
      case 19: // NOTE
        value.template destroy< std::string > ();
        break;

      default:
        break;
    }

    Base::clear ();
  }

  template <typename Base>
  inline
  bool
  InternParser::basic_symbol<Base>::empty () const
  {
    return Base::type_get () == empty_symbol;
  }

  template <typename Base>
  inline
  void
  InternParser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move(s);
      switch (this->type_get ())
    {
      case 18: // NAME
      case 19: // NOTE
        value.move< std::string > (s.value);
        break;

      default:
        break;
    }

    location = s.location;
  }

  // by_type.
  inline
  InternParser::by_type::by_type ()
    : type (empty_symbol)
  {}

  inline
  InternParser::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  inline
  InternParser::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  inline
  void
  InternParser::by_type::clear ()
  {
    type = empty_symbol;
  }

  inline
  void
  InternParser::by_type::move (by_type& that)
  {
    type = that.type;
    that.clear ();
  }

  inline
  int
  InternParser::by_type::type_get () const
  {
    return type;
  }
  // Implementation of make_symbol for each symbol type.
  InternParser::symbol_type
  InternParser::make_CREATE (const location_type& l)
  {
    return symbol_type (token::CREATE, l);
  }

  InternParser::symbol_type
  InternParser::make_START (const location_type& l)
  {
    return symbol_type (token::START, l);
  }

  InternParser::symbol_type
  InternParser::make_LIST (const location_type& l)
  {
    return symbol_type (token::LIST, l);
  }

  InternParser::symbol_type
  InternParser::make_ADD (const location_type& l)
  {
    return symbol_type (token::ADD, l);
  }

  InternParser::symbol_type
  InternParser::make_REMOVE (const location_type& l)
  {
    return symbol_type (token::REMOVE, l);
  }

  InternParser::symbol_type
  InternParser::make_DESTROY (const location_type& l)
  {
    return symbol_type (token::DESTROY, l);
  }

  InternParser::symbol_type
  InternParser::make_QUIT (const location_type& l)
  {
    return symbol_type (token::QUIT, l);
  }

  InternParser::symbol_type
  InternParser::make_CHORD (const location_type& l)
  {
    return symbol_type (token::CHORD, l);
  }

  InternParser::symbol_type
  InternParser::make_SECTION (const location_type& l)
  {
    return symbol_type (token::SECTION, l);
  }

  InternParser::symbol_type
  InternParser::make_MUSIC (const location_type& l)
  {
    return symbol_type (token::MUSIC, l);
  }

  InternParser::symbol_type
  InternParser::make_SETLIST (const location_type& l)
  {
    return symbol_type (token::SETLIST, l);
  }

  InternParser::symbol_type
  InternParser::make_OPBKT (const location_type& l)
  {
    return symbol_type (token::OPBKT, l);
  }

  InternParser::symbol_type
  InternParser::make_CLBKT (const location_type& l)
  {
    return symbol_type (token::CLBKT, l);
  }

  InternParser::symbol_type
  InternParser::make_COMMA (const location_type& l)
  {
    return symbol_type (token::COMMA, l);
  }

  InternParser::symbol_type
  InternParser::make_QUOTE (const location_type& l)
  {
    return symbol_type (token::QUOTE, l);
  }

  InternParser::symbol_type
  InternParser::make_NAME (const std::string& v, const location_type& l)
  {
    return symbol_type (token::NAME, v, l);
  }

  InternParser::symbol_type
  InternParser::make_NOTE (const std::string& v, const location_type& l)
  {
    return symbol_type (token::NOTE, v, l);
  }

  InternParser::symbol_type
  InternParser::make_NC (const location_type& l)
  {
    return symbol_type (token::NC, l);
  }

  InternParser::symbol_type
  InternParser::make_END (const location_type& l)
  {
    return symbol_type (token::END, l);
  }



  // by_state.
  inline
  InternParser::by_state::by_state ()
    : state (empty_state)
  {}

  inline
  InternParser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  InternParser::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
  InternParser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
  InternParser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  InternParser::symbol_number_type
  InternParser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
  InternParser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  InternParser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 18: // NAME
      case 19: // NOTE
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
  InternParser::stack_symbol_type&
  InternParser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 18: // NAME
      case 19: // NOTE
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
  InternParser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  InternParser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  inline
  void
  InternParser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  InternParser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  InternParser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  InternParser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  InternParser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  InternParser::debug_level_type
  InternParser::debug_level () const
  {
    return yydebug_;
  }

  void
  InternParser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline InternParser::state_type
  InternParser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
  InternParser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  InternParser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  InternParser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            yyla.type = yytranslate_ (yylex (&yyla.value, &yyla.location));
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 18: // NAME
      case 19: // NOTE
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }


      // Compute the default @$.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:
#line 137 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 880 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 3:
#line 138 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 886 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 4:
#line 139 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 892 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 5:
#line 140 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 898 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 6:
#line 141 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 904 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 7:
#line 142 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 910 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 8:
#line 143 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 916 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 9:
#line 144 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 922 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 10:
#line 145 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 928 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 11:
#line 146 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 934 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 12:
#line 147 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 940 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 13:
#line 148 "TextUiParser.y" // lalr1.cc:859
    {return 0;}
#line 946 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 16:
#line 155 "TextUiParser.y" // lalr1.cc:859
    {helper.createChord();}
#line 952 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 18:
#line 161 "TextUiParser.y" // lalr1.cc:859
    {helper.createSection();}
#line 958 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 20:
#line 167 "TextUiParser.y" // lalr1.cc:859
    {helper.createMusic();}
#line 964 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 22:
#line 173 "TextUiParser.y" // lalr1.cc:859
    {helper.createSetlist();}
#line 970 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 24:
#line 179 "TextUiParser.y" // lalr1.cc:859
    {helper.start();}
#line 976 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 25:
#line 182 "TextUiParser.y" // lalr1.cc:859
    {helper.list();}
#line 982 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 26:
#line 185 "TextUiParser.y" // lalr1.cc:859
    {helper.add();}
#line 988 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 27:
#line 188 "TextUiParser.y" // lalr1.cc:859
    {helper.remove();}
#line 994 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 28:
#line 191 "TextUiParser.y" // lalr1.cc:859
    {helper.destroy();}
#line 1000 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 29:
#line 194 "TextUiParser.y" // lalr1.cc:859
    {helper.quit();}
#line 1006 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 33:
#line 204 "TextUiParser.y" // lalr1.cc:859
    {helper.addName(yystack_[0].value.as< std::string > ());}
#line 1012 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 34:
#line 207 "TextUiParser.y" // lalr1.cc:859
    {helper.addNote(yystack_[0].value.as< std::string > ());}
#line 1018 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;

  case 35:
#line 210 "TextUiParser.y" // lalr1.cc:859
    {yyclearin;
			yyerrok;}
#line 1025 "TextUiParser.tab.cpp" // lalr1.cc:859
    break;


#line 1029 "TextUiParser.tab.cpp" // lalr1.cc:859
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  InternParser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
  InternParser::yysyntax_error_ (state_type yystate, const symbol_type& yyla) const
  {
    // Number of reported tokens (one for the "unexpected", one per
    // "expected").
    size_t yycount = 0;
    // Its maximum.
    enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
    // Arguments of yyformat.
    char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];

    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yyla) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state
         merging (from LALR or IELR) and default reductions corrupt the
         expected token list.  However, the list is correct for
         canonical LR with one exception: it will still contain any
         token that will not be accepted due to an error action in a
         later state.
    */
    if (!yyla.empty ())
      {
        int yytoken = yyla.type_get ();
        yyarg[yycount++] = yytname_[yytoken];
        int yyn = yypact_[yystate];
        if (!yy_pact_value_is_default_ (yyn))
          {
            /* Start YYX at -YYN if negative to avoid negative indexes in
               YYCHECK.  In other words, skip the first -YYN actions for
               this state because they are default actions.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;
            // Stay within bounds of both yycheck and yytname.
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
              if (yycheck_[yyx + yyn] == yyx && yyx != yyterror_
                  && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
                {
                  if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                    {
                      yycount = 1;
                      break;
                    }
                  else
                    yyarg[yycount++] = yytname_[yyx];
                }
          }
      }

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
        YYCASE_(0, YY_("syntax error"));
        YYCASE_(1, YY_("syntax error, unexpected %s"));
        YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    std::string yyres;
    // Argument number.
    size_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += yytnamerr_ (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const signed char InternParser::yypact_ninf_ = -30;

  const signed char InternParser::yytable_ninf_ = -1;

  const signed char
  InternParser::yypact_[] =
  {
       1,   -30,   -13,    -3,     6,    15,    15,    16,    17,   -30,
      32,    -4,    18,    21,    25,    23,    25,    24,    25,    26,
      27,    28,    29,    30,    31,    33,     2,    38,    40,    25,
     -30,   -30,    25,    41,   -30,   -30,   -30,   -30,   -30,     8,
     -30,   -30,    10,   -30,    12,   -30,   -30,   -30,   -30,   -30,
     -30,   -30,    20,    42,    43,    44,   -30,   -30,    45,    47,
     -30,    -1,   -30,    25,   -30,   -30,    25,    25,    25,    25,
      25,   -30,    48,   -30,   -30,   -30,    49,    50,    51,    52,
      54,   -30,   -30,   -30,   -30,   -30,   -30
  };

  const unsigned char
  InternParser::yydefact_[] =
  {
       0,    35,     0,     0,     0,     0,     0,     0,     0,    13,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      26,    27,     0,     0,     1,     2,    30,     3,    33,     0,
      14,     4,     0,     5,     0,     6,     7,     8,     9,    10,
      11,    12,     0,     0,     0,     0,    24,    25,     0,     0,
      29,     0,    18,     0,    20,    22,     0,     0,     0,     0,
       0,    28,     0,    34,    31,    15,     0,     0,     0,     0,
       0,    16,    17,    19,    21,    23,    32
  };

  const signed char
  InternParser::yypgoto_[] =
  {
     -30,   -30,     3,   -30,   -30,   -30,   -30,   -30,   -30,   -30,
     -30,   -30,   -30,   -30,   -30,   -30,   -30,   -30,    58,   -29,
     -30,   -30
  };

  const signed char
  InternParser::yydefgoto_[] =
  {
      -1,    10,    39,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    61,    30,    40,
      74,    25
  };

  const unsigned char
  InternParser::yytable_[] =
  {
      58,    26,     1,    59,     2,     3,     4,     5,     6,     7,
       8,    27,    52,    53,    54,    55,    72,    35,    73,    42,
      28,    44,     9,    62,    63,    64,    63,    65,    63,    29,
      32,    33,    34,     0,    75,    36,    66,    76,    77,    78,
      79,    80,    37,    38,    41,    43,     0,    45,    46,    47,
      48,    49,    50,    56,    51,    57,    60,     0,    67,    68,
      69,    70,    71,    81,    31,    82,    83,    84,    85,    86
  };

  const signed char
  InternParser::yycheck_[] =
  {
      29,    14,     1,    32,     3,     4,     5,     6,     7,     8,
       9,    14,    10,    11,    12,    13,    17,    21,    19,    16,
      14,    18,    21,    15,    16,    15,    16,    15,    16,    14,
      14,    14,     0,    -1,    63,    17,    16,    66,    67,    68,
      69,    70,    21,    18,    21,    21,    -1,    21,    21,    21,
      21,    21,    21,    15,    21,    15,    15,    -1,    16,    16,
      16,    16,    15,    15,     6,    16,    16,    16,    16,    15
  };

  const unsigned char
  InternParser::yystos_[] =
  {
       0,     1,     3,     4,     5,     6,     7,     8,     9,    21,
      23,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    43,    14,    14,    14,    14,
      40,    40,    14,    14,     0,    21,    17,    21,    18,    24,
      41,    21,    24,    21,    24,    21,    21,    21,    21,    21,
      21,    21,    10,    11,    12,    13,    15,    15,    41,    41,
      15,    39,    15,    16,    15,    15,    16,    16,    16,    16,
      16,    15,    17,    19,    42,    41,    41,    41,    41,    41,
      41,    15,    16,    16,    16,    16,    15
  };

  const unsigned char
  InternParser::yyr1_[] =
  {
       0,    22,    23,    23,    23,    23,    23,    23,    23,    23,
      23,    23,    23,    23,    24,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    39,    40,    41,    42,    43
  };

  const unsigned char
  InternParser::yyr2_[] =
  {
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     1,     1,     3,     5,     6,     3,     6,
       3,     6,     3,     6,     3,     3,     2,     2,     4,     3,
       0,     2,     5,     1,     1,     1
  };



  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const InternParser::yytname_[] =
  {
  "$end", "error", "$undefined", "CREATE", "START", "LIST", "ADD",
  "REMOVE", "DESTROY", "QUIT", "CHORD", "SECTION", "MUSIC", "SETLIST",
  "OPBKT", "CLBKT", "COMMA", "QUOTE", "NAME", "NOTE", "NC", "END",
  "$accept", "command", "nameList", "createChord", "createChordPrefix",
  "createSection", "createSectionPrefix", "createMusic",
  "createMusicPrefix", "createSetlist", "createSetlistPrefix", "start",
  "list", "add", "remove", "destroy", "quit", "chord", "pair", "name",
  "note", "err", YY_NULLPTR
  };

#if YYDEBUG
  const unsigned char
  InternParser::yyrline_[] =
  {
       0,   137,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   151,   152,   155,   158,   161,   164,
     167,   170,   173,   176,   179,   182,   185,   188,   191,   194,
     197,   198,   201,   204,   207,   210
  };

  // Print the state stack on the debug stream.
  void
  InternParser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  InternParser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG

  // Symbol number corresponding to token number t.
  inline
  InternParser::token_number_type
  InternParser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21
    };
    const unsigned int user_token_number_max_ = 276;
    const token_number_type undef_token_ = 2;

    if (static_cast<int>(t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned int> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }

#line 4 "TextUiParser.y" // lalr1.cc:1167
} // TextUi
#line 1495 "TextUiParser.tab.cpp" // lalr1.cc:1167
#line 213 "TextUiParser.y" // lalr1.cc:1168


