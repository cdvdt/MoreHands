%skeleton "lalr1.cc"
%require  "3.0"
%defines 
%define api.namespace {TextUi}
%define parser_class_name {InternParser}
%define parse.error verbose

%parse-param {TextUi::Lexer &lexer}
/*%parse-param {std::vector<CommandRequest *> &commandList}*/
%parse-param {TextUi::InternalParserHelper &helper}

%code requires {
#include <vector>
#include <string>
#include "CommandRequest.h"

#define  TEXTUIPARSER_TAB_HPP
	
namespace TextUi {
	class Lexer;
	
	class InternalParserHelper {
	public:
		void setCommandList (std::vector<CommandRequest *> &commandList);
		void addNote(std::string note);
		void addName(std::string name);
		void createChord ();
		void createSection ();
		void createMusic ();
		void createSetlist ();
		void start ();
		void list ();
		void add ();
		void remove ();
		void destroy ();
		void quit();		
	private:
		std::vector<CommandRequest*> *commandList;
	};
}

// The following definitions is missing when %locations isn't used
# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

}

%code{
   #include <iostream>
   #include <cstdlib>
   #include <fstream>
   #include "CommandRequest.h"
   #include "TextUiLexer.h"
   
   /* include for all driver functions */
   //#include "mc_driver.hpp"

void addNote(std::string note) {
	std::cout << "a note " << note << std::endl;
}

void addName(std::string name) {
	std::cout << "a name " << name << std::endl;
}

void createChord () {
	std::cout << "a chord " << std::endl;
}

void createSection () {
	std::cout << "a section " << std::endl;
}

void createMusic () {
	std::cout << "a music " << std::endl;
}

void createSetlist () {
	std::cout << "a setlist " << std::endl;
}

void start () {
	std::cout << "start!" << std::endl;
}

void list () {
	std::cout << "list!" << std::endl;
}

void add () {
	std::cout << "adicionando " << std::endl;
}

void remove () {
	std::cout << "removendo " << std::endl;
}

void destroy () {
	std::cout << "destroi " << std::endl;
}

void quit() {
	exit(0);
}

#undef yylex
#define yylex lexer.yylex
}

%define api.value.type variant
/*%define parse.assert*/

/*tokens*/
/*commands*/
%token CREATE START LIST ADD REMOVE DESTROY QUIT
/*types*/ 
%token CHORD SECTION MUSIC SETLIST
/*tokens*/
%token OPBKT CLBKT COMMA QUOTE
/*data*/
%token <std::string> NAME
%token <std::string> NOTE

%token NC END

%locations

%define parse.trace
%define parse.assert

%%
command: createChord END	{return 0;}
	   | createSection END	{return 0;}
	   | createMusic END	{return 0;}
	   | createSetlist END	{return 0;}
	   | start END			{return 0;}
	   | list END			{return 0;}
	   | add END			{return 0;}
	   | remove END			{return 0;}
	   | destroy END		{return 0;}
	   | quit END			{return 0;}
	   | err END			{return 0;}
	   | END				{return 0;}
	   ;
	   			
nameList: name
		| nameList COMMA name
		;
	   			
createChord: createChordPrefix QUOTE chord QUOTE CLBKT	{helper.createChord();}
		   ;
		   
createChordPrefix: CREATE OPBKT CHORD COMMA name COMMA
				 ;

createSection: createSectionPrefix nameList CLBKT	{helper.createSection();}
			 ;

createSectionPrefix: CREATE OPBKT SECTION COMMA name COMMA
				   ;
			 
createMusic: createMusicPrefix nameList CLBKT	{helper.createMusic();}
		   ;
		   
createMusicPrefix: CREATE OPBKT MUSIC COMMA name COMMA
				 ;

createSetlist: createSetlistPrefix nameList CLBKT	{helper.createSetlist();}
		     ;
		     
createSetlistPrefix: CREATE OPBKT SETLIST COMMA name COMMA
				   ;			 

start: START OPBKT CLBKT	{helper.start();}
	 ;
	 
list: LIST OPBKT CLBKT	{helper.list();}
	;

add: ADD pair	{helper.add();}
   ;
   
remove: REMOVE pair	{helper.remove();}
	  ;

destroy: DESTROY OPBKT name CLBKT	{helper.destroy();}
	   ;
	   
quit: QUIT OPBKT CLBKT	{helper.quit();}
	;
	   
chord:
	 | chord note
	 ;
	   
pair: OPBKT name COMMA name CLBKT
	;

name: NAME 	{helper.addName($1);}
	;
	
note: NOTE	{helper.addNote($1);}
	;

err: error {yyclearin;
			yyerrok;}
   ;
%%

