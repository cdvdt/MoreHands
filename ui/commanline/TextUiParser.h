/*
 * TextUiParser.h
 *
 *  Created on: 07/07/2017
 *      Author: cdvdt
 */

#ifndef TEXTUIPARSER_H_
#define TEXTUIPARSER_H_

#include <vector>
#include "TextUiParser.tab.hpp"
#include "TextUiLexer.h"
#include <sstream>
#include "CommandRequest.h"

namespace TextUi {

class TextUiParser {
public:
	TextUiParser();
	TextUiParser(std::stringstream &stream);
	virtual ~TextUiParser();

	int parse(std::vector<CommandRequest *> &commands);

private:
	InternParser *parser;
	InternalParserHelper *helper;
	Lexer *lexer;
	std::vector<CommandRequest> *commands;
};

} /* namespace TextUi */

#endif /* TEXTUIPARSER_H_ */
