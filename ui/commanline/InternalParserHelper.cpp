/*
 * InternalParserHelper.cpp
 *
 *  Created on: 08/07/2017
 *      Author: cdvdt
 */

#include "TextUiParser.tab.hpp"

namespace TextUi {
void InternalParserHelper::setCommandList(std::vector<CommandRequest *> &commandList) {
	this->commandList = &commandList;
}
void InternalParserHelper::addNote(std::string note) {
	std::cout << "a note " << note << std::endl;
	if (commandList->size() > 0) {
		if(commandList->back()->isOpen == false) {
			commandList->push_back(new CommandRequest);
		}
	} else {
		commandList->push_back(new CommandRequest);
	}
	commandList->back()->data.push_back(note);
}

void InternalParserHelper::addName(std::string name) {
	std::cout << "a name " << name << std::endl;
	if (commandList->size() > 0) {
		if(commandList->back()->isOpen == false) {
			commandList->push_back(new CommandRequest);
		}
	} else {
		commandList->push_back(new CommandRequest);
	}
	commandList->back()->data.push_back(name);
}

void InternalParserHelper::createChord () {
	std::cout << "a chord " << std::endl;
	commandList->back()->dataType = CHORD;
	commandList->back()->type = CREATE;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::createSection () {
	std::cout << "a section " << std::endl;
	commandList->back()->dataType = SECTION;
	commandList->back()->type = CREATE;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::createMusic () {
	std::cout << "a music " << std::endl;
	commandList->back()->dataType = MUSIC;
	commandList->back()->type = CREATE;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::createSetlist () {
	std::cout << "a setlist " << std::endl;
	commandList->back()->dataType = SETLIST;
	commandList->back()->type = CREATE;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::start () {
	std::cout << "start!" << std::endl;
	commandList->push_back(new CommandRequest);
	commandList->back()->type = START;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::list () {
	std::cout << "list!" << std::endl;
	commandList->push_back(new CommandRequest);
	commandList->back()->type = LIST;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::add () {
	std::cout << "adicionando " << std::endl;
	commandList->back()->type = ADD;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::remove () {
	std::cout << "removendo " << std::endl;
	commandList->back()->type = REMOVE;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::destroy () {
	std::cout << "destroi " << std::endl;
	commandList->back()->type = DESTROY;
	commandList->back()->isOpen = false;
}

void InternalParserHelper::quit () {
	std::cout << "saindo " << std::endl;
	commandList->back()->type = QUIT;
	commandList->back()->isOpen = false;
	exit(0);//must be removed later
}

void InternParser::error(const location_type& location, const std::string &errorMessage) {
	std::cout << "a mensagem de erro foi: " << errorMessage << std::endl;
}


}
