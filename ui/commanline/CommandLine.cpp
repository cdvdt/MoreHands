/*
 * CommandLine.cpp
 *
 *  Created on: 22/07/2017
 *      Author: cdvdt
 */

#include "CommandLine.h"

CommandLine::CommandLine() {
	// TODO Auto-generated constructor stub
	parser = new TextUi::TextUiParser(input);
	currentCommands.push_back(new CommandRequest());
}

void CommandLine::readCommand() {
	char *line = readline(parsePrompt());
	input.flush();
	input << line << std::endl;
	parser->parse(currentCommands);
	std::cout << currentCommands.size() << std::endl;
	add_history(line);
	free(line);
}

CommandLine::CommandLine(std::string promptFormat) : promptFormat(promptFormat) {
	parser = new TextUi::TextUiParser(input);
}

const std::string& CommandLine::getPromptFormat() const {
	return promptFormat;
}

void CommandLine::setPromptFormat(const std::string& promptFormat) {
	this->promptFormat = promptFormat;
}

void CommandLine::show() {
	std::cout << "MoreHands version " << "version" << std::endl << "licença" << std::endl << std::endl << "Written by Sergio Filho" << std::endl;
	while(!isQuitRequested()) {
		readCommand();
	}
}

const char* CommandLine::parsePrompt() {
	return promptFormat.c_str();
}

bool CommandLine::isQuitRequested() {
	return false;
}
