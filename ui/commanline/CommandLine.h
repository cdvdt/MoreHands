/*
 * CommandLine.h
 *
 *  Created on: 22/07/2017
 *      Author: cdvdt
 */

#ifndef COMMANDLINE_H_
#define COMMANDLINE_H_

#include "CommandRequest.h"
#include <readline/readline.h>
#include <readline/history.h>
#include <iostream>
#include <sstream>
#include <vector>
#include "TextUiParser.h"

class CommandLine {
public:
	CommandLine();
	CommandLine(std::string promptFormat);

	void show();

	void readCommand();
	const std::string& getPromptFormat() const;
	void setPromptFormat(const std::string& promptFormat);

private:
	std::vector<CommandRequest *> currentCommands;
	std::stringstream input;
	std::string promptFormat;
	TextUi::TextUiParser *parser;

	const char* parsePrompt();
	bool isQuitRequested();
};

#endif /* COMMANDLINE_H_ */
