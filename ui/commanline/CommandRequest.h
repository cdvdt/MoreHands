/*
 * CommandRequest.h
 *
 *  Created on: 04/07/2017
 *      Author: cdvdt
 */

#ifndef COMMANDREQUEST_H_
#define COMMANDREQUEST_H_

#include <vector>
#include <string>

enum CommandType {
	CREATE,
	START,
	LIST,
	ADD,
	REMOVE,
	DESTROY,
	QUIT
};

enum CommandDataType {
	CHORD,
	SECTION,
	MUSIC,
	SETLIST
};

struct CommandRequest {
	enum CommandType type;
	enum CommandDataType dataType;
	std::vector<std::string> data;
	bool isOpen;

	CommandRequest() : type(CREATE), dataType(CHORD), isOpen(true) {

	}
	std::string toString() {
		std::string sData;
		if (data.size() > 1) for (uint i = 0; i < data.size() - 1; i++) sData += data[i] + ", ";
		if (data.size() > 0) sData += data.back();
		return "Type: " + std::to_string(type) + ", Data type:" + std::to_string(dataType) + ", Data: " + sData;
	}
};


#endif /* COMMANDREQUEST_H_ */
