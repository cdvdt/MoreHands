/*
 * TextUiLexer.h
 *
 *  Created on: 04/07/2017
 *      Author: cdvdt
 */

#ifndef TEXTUILEXER_H_
#define TEXTUILEXER_H_

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#ifndef TEXTUIPARSER_TAB_HPP
	#define  TEXTUIPARSER_TAB_HPP
	#include "TextUiParser.tab.hpp"
#endif
#include "location.hh"

namespace TextUi {
class Lexer: public yyFlexLexer {
public:
	Lexer(std::istream *in) : yyFlexLexer(in)
	{
	      loc = new TextUi::InternParser::location_type();
	};
	using FlexLexer::yylex;

	virtual int yylex(TextUi::InternParser::semantic_type* yylval, TextUi::InternParser::location_type *location);
private:
	/* yyval ptr */
	TextUi::InternParser::semantic_type *yylval = nullptr;
	/* location ptr */
	TextUi::InternParser::location_type *loc = nullptr;
};
}

#endif /* TEXTUILEXER_H_ */
