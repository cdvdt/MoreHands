%{
#include "TextUiLexer.h"
#undef YY_DECL
#define YY_DECL int TextUi::Lexer::yylex(TextUi::InternParser::semantic_type* yylval, TextUi::InternParser::location_type *location)

#define yyterminate() return (token::END)

/* update location on matching */
#define YY_USER_ACTION location->step(); location->columns(yyleng);

/* using "token" to make the returns for the tokens shorter to type */
using token = TextUi::InternParser::token;
%}

%option c++
%option yyclass="TextUi::Lexer"
%option noyywrap
%option caseless
%option debug

%%
%{
	this->yylval = yylval;
	//this->loc = location;
%}

						/*commands*/
"create"				{return token::CREATE;}
"start"					{return token::START;}
"list"					{return token::LIST;}
"add"					{return token::ADD;}
"remove"				{return token::REMOVE;}
"destroy"				{return token::DESTROY;}
"quit"					{return token::QUIT;}

						/*types*/
"chord"					{return token::CHORD;}
"section"				{return token::SECTION;}
"music"					{return token::MUSIC;}
"setlist"				{return token::SETLIST;}

						/*tokens*/
\(						{return token::OPBKT;}
\)						{return token::CLBKT;}
,						{return token::COMMA;}
\"						{return token::QUOTE;}

						/*data*/
[a-z][#b]?[+-]?1?[0-9]	{yylval->build<std::string>() = yytext; return token::NOTE;}
[a-z][a-z0-9]*			{yylval->build<std::string>() = yytext; return token::NAME;}

						/*other*/
" "						{}
\n						{return token::END;}
%%
