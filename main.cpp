/*
 *  main.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef USE_QT_GUI
#include "mainwindow.h"
#include <QApplication>
#endif

#include "model/chord.h"
#include "model/section.h"
#include "controller/engine.h"
#include "ui/commanline/CommandLine.h"
#include <iostream>
#include "model/alias.h"
#include "model/events/midiSide/DefaultMidiEvent.h"
#include "model/events/systemSide/KeyboardEvent.h"

int main(int argc, char *argv[])
{
    Engine engine;

    engine.exec();
	DefaultMidiEvent midiEvent;
	KeyboardEvent key('e', KeyboardEvent::NONE);
	KeyboardEvent key2('e', KeyboardEvent::NONE);
	Alias alias1(key);
	Alias alias2(key2);
	std::cout << (alias1 == midiEvent) << std::endl;
    CommandLine commandLine("ready: ");
    commandLine.show();

    //while(1);

#ifdef USE_QT_GUI
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
#endif
    /*Section section;

    section.addElement(new Chord("A2 E3 C4 G4", "Am"));
    section.addElement(new Chord("F2 C3 A4 F5", "F"));
    section.addElement(new Chord("C2 G2 E4 C5", "C"));
    section.addElement(new Chord("G2 D3 B4 G5", "G"));

    std::cout << section.getContent();*/


}
