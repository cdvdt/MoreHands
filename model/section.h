/*
 *  section.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SECTION_H
#define SECTION_H

#include <vector>
#include "abstractelement.h"
#include "abstractsection.h"

/**
 * Class that holds elements from a section, which is part of a music. Elements must be derived from AbstractElement, as it defines the methods necessary in this class.
 * It is derived from AbstractSection, like Music and Setlist classes.
 */

class Section : public AbstractSection
{
public:
    Section();
    /**
     * Initiates this section, resets the current element to the first element.
     */
    virtual void startSection();
    /**
     * Returns the element pointed by this section. If currentElement is not valid (exceeds the number of elements held by this section) it returns a nullptr.
     * \return Element pointed by currentElement or nullpt if invalid
     */
    virtual AbstractElement* getCurrent();
    /**
     * Changes current element to next element to be used.
     * \return true if currentElement points to a valid element and false otherwise
     */
    virtual bool stepForward();
    /**
     * Amount of elements held by this object
     * \return Amount of elements held by this object
     */
    virtual int size();
    /**
     * Adds an element to the end of the list
     * \param element element to be added to the list
     */
    void addElement(AbstractElement* element);
    /**
     * Adds an element to the list at position indicated by index.
     * \param element element to be added to the list
     * \param index position where it will be added
     */
    void addElement(AbstractElement* element, int index);
    /**
     * Returns the element at index position
     * \param index position of the object to be returned
     * \return Element at position index
     */
    AbstractElement *getElementAt(int index);
    /**
     * Returns a string representation of the content of the elements list
     * \return representation of the content of the elements list.
     */
    std::string getContent();

    /**
     * Returns the element at index position
     * \param index position of the object to be returned
     * \return Element at position index
     */
    AbstractElement* operator[] (int index);
    
private:
    std::vector<AbstractElement*> elements; ///< Elements held by a section
    int currentElement; ///< Current element
};

#endif // SECTION_H
