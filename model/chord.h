/*
 *  chord.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHORD_H
#define CHORD_H

#include "abstractelement.h"
#include <string>
#include <vector>

/**
 * Describes a chord by it's notes distributed over the octaves.
 */
class Chord : public AbstractElement
{
public:
    /**
     * Returns the content of this object in the format [note][octave] [note][octave]...[note][octave].
     * \return the content of this object
     */
    virtual std::string getContent();
    /**
     * Constructor
     * \param notes List of notes format [note][octave] [note][octave]...[note][octave]
     * \param name Name associated with this object
     * \param surmane Surname associated with this object
     */
    Chord(std::string notes, std::string name, std::string surmane);
    /**
     * Constructor
     * \param notes List of notes format [note][octave] [note][octave]...[note][octave]
     * \param name Name associated with this object
     */
    Chord(std::string notes, std::string name);
    /**
     * Constructor
     * \param notes List of notes format [note][octave] [note][octave]...[note][octave]
     */
    Chord(std::string notes);
    /**
     * Sets the list of notes for the chord.
     * \param notes List of notes format [note][octave] [note][octave]...[note][octave]
     */
    void setChordNotes(std::string notes);
    /**
     * Returns an array of integers with the note represented as it's integer MIDI value.
     * @return
     */
    int* getChordNotes();
    /**
     * Returns the notes of this chord in the format [note][octave] [note][octave]...[note][octave].
     * \return the notes of this chord
     */
    std::string getNotesAsString();

private:
    std::vector<int> chordNotes; ///< list of notes in it's integer MIDI representation
};

#endif // CHORD_H
