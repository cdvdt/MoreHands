/*
 *  setlist.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETLIST_H
#define SETLIST_H

#include "abstractsection.h" // it should not be AbstractSection, may be a pure class
#include "music.h"
#include <vector>

/**
 * Defines a list of Music musics.
 */
class Setlist {
public:
	Setlist();

	/**
	 * \deprecated
	 */
	Music *getCurrent();
	/**
	 * \deprecated
	 */
	bool stepForward();
	/**
	 * \deprecated
	 */
	void updateCurrent(Music *music, Section *Section);
	/**
	 * Amount of elements held by this object
	 * \return Amount of elements held by this object
	 */
	int size();
	/**
	 * add a new Music to this Setlist. If an index is specified it inserts the new element at index position, else adds to the end of the list.
	 * \param newMusic Music to be added
	 * \param index position for newMusic to be added, if unspecified adds to the end of the list;
	 */
	void addMusic(Music* newMusic, int index = -1);
	/**
	 * Removes a Music indicated by its index from this Setlist.
	 * \param index position of the item to be removed
	 */
	void removeMusic(int index);
	/**
	 * Returns an array with the content of this setlist.
	 * \return array with the content of this setlist
	 */
	Music* *getSetlist();
	/**
	 * \deprecated
	 */
	int getSetlistSize();
	/**
	 * Returns the Music at index position
	 * \param index position of the object to be returned
	 * \return Music at position index
	 */
	Music* operator[](int index);
	/**
	 * returns a string representation of the content of this setlist.
	 * \return a string representation of the content of this setlist.
	 */
	std::string getContent();
private:
	std::vector<Music *> setlist;
};

#endif // SETLIST_H
