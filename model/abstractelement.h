/*
 *  abstractelement.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABSTRACTELEMENT_H
#define ABSTRACTELEMENT_H

#include <string>

/**
 * Defines a base class from which Chord, ControlChanges and Fragment are derived, these classes are held by Section so they must have AbstractElement as parent, as Section uses methods defined in this class.
 */
class AbstractElement
{
public:
    AbstractElement();

public:
    /**
     * Returns the name of this element
     */
    virtual std::string getName();
    /**
     * Sets the name for this element.
     * \param name name to be associated to this element
     */
    virtual void setName(std::string name);
    /**
     * Returns the surname for this element. Maybe necessary as some elements, like Chords, may have the same name, even if it's content is not the same.
     */
    virtual std::string getSurname();
    /**
     * Returns the surname for this element. Maybe necessary as some elements, like Chords, may have the same name, even if it's content is not the same.
     * \param surname surname to be associated to this element
     */
    virtual void setSurname(std::string surname);
    /**
     * Returns a string representation of this element
     * \return representation of this element.
     */
    virtual std::string getContent() = 0;

protected:
    std::string name; ///< name associated with this object
    std::string surname; ///< surname associated with this object

};

#endif // ABSTRACTELEMENT_H
