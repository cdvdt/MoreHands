/*
 *  chord.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chord.h"

#include <cctype>
#include <algorithm>
#include <cstdlib>

//for debugging
#ifndef QT_NO_DEBUG
#include <iostream>
#endif

int parseNote(std::string notes, int begin, int *end) {
    int note;
    int multiplier = 1;
    if((notes.length()) - begin >= 2) {
        switch (tolower(notes[begin])){
        case 'a':
            note = 9;
            break;
        case 'b':
            note = 11;
            break;
        case 'c':
            note = 0;
            break;
        case 'd':
            note = 2;
            break;
        case 'e':
            note = 4;
            break;
        case 'f':
            note = 5;
            break;
        case 'g':
            note = 7;
            break;
        default:
            return -1;
        }
    } else {
        return -1;
    }
    begin++;
    switch (notes[begin]){
    case '#':
        note += 1;
        begin++;
        break;
    case 'b':
        note -= 1;
        begin++;
        break;
    }
    switch (notes[begin]){
    case '-':
        multiplier = -1;
        begin++;
        break;
    }
    if(((notes.length()) - begin >= 1) && (isdigit(notes[begin]))) {
        note += (notes[begin] - '0') * 12 * multiplier + 24;
        *end = begin + 1;
        return note;
    } else {
        return -1;
    }
}

void Chord::setChordNotes(std::string notes)
{
    //QString cleanNotes = notes.remove(' ');
    notes.erase(std::remove_if(notes.begin(), notes.end(), [] (char x) {return std::isspace(x);}));

    int begin = 0;
    int newNote = 0;
    while (begin < (int) notes.length() && newNote != -1) {
#ifndef QT_NO_DEBUG
        std::cout << "begin: " << begin << std::endl;
#endif
        newNote = parseNote(notes, begin, &begin);
#ifndef QT_NO_DEBUG
        std::cout << "note: " << newNote << std::endl;
#endif
        if (newNote != -1)
            chordNotes.push_back(newNote);
    }
}

std::string noteAsString(int note, bool useFlat) {
    std::string result;
    note += (note < 0 ? 12 : 0);
    if(!useFlat) {
        switch (note % 12) {
        case 0:
            result += "C";
            break;
        case 1:
            result += "C#";
            break;
        case 2:
            result += "D";
            break;
        case 3:
            result += "D#";
            break;
        case 4:
            result += "E";
            break;
        case 5:
            result += "F";
            break;
        case 6:
            result += "F#";
            break;
        case 7:
            result += "G";
            break;
        case 8:
            result += "G#";
            break;
        case 9:
            result += "A";
            break;
        case 10:
            result += "A#";
            break;
        case 11:
            result += "B";
            break;
        default:
            break;
        }
    } else {
        switch (note % 12) {
        case 0:
            result += "C";
            break;
        case 1:
            result += "Db";
            break;
        case 2:
            result += "D";
            break;
        case 3:
            result += "Eb";
            break;
        case 4:
            result += "E";
            break;
        case 5:
            result += "F";
            break;
        case 6:
            result += "Gb";
            break;
        case 7:
            result += "G";
            break;
        case 8:
            result += "Ab";
            break;
        case 9:
            result += "A";
            break;
        case 10:
            result += "Bb";
            break;
        case 11:
            result += "B";
            break;
        default:
            break;
        }
    }

    return result + std::to_string(note / 12 - 2);
}

std::string notesAsString(std::vector<int> notes) {
    std::string result;
    for(int i = 0; i < (int) notes.size() - 1; i++) {
        result += noteAsString(notes[i], false);
        if (i < (int) notes.size() - 2)
            result += " ";
    }
    return result;
}

std::string Chord::getNotesAsString() {
    return notesAsString(chordNotes);
}

std::string Chord::getContent() {
    return getNotesAsString();
}

Chord::Chord(std::string notes) {
    setChordNotes(notes);
}

Chord::Chord(std::string notes, std::string name) {
    this->name = name;
    setChordNotes(notes);
}

Chord::Chord(std::string notes, std::string name, std::string surname) {
    this->name = name;
    this->surname = surname;
    setChordNotes(notes);
}

