/*
 *  alias.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALIAS_H
#define ALIAS_H

#include <functional>
#include "events/MoreHandsEvent.h"

/**
 * Represents a shortcut to a Section or Music. It may be a key, a group of keys, or a MIDI signal.
 */

class Alias
{
public:
	Alias();
    Alias(MoreHandsEvent &event);

    /**
     * not implemented yet
     * \param other alias to be compared
     * \return result of a comparation between to aliases
     */
    bool operator==(const Alias &other) const;

private:
    MoreHandsEvent *event;
};

/*
 * Definition of hash for Alias. Not implemented yet
 */
namespace std {

template <>
struct hash<Alias>
{
    std::size_t operator()(const Alias& k) const
    {
        using std::size_t;
        using std::hash;
        using std::string;

        // Compute individual hash values for first,
        // second and third and combine them using XOR
        // and bit shifting:

        return 0;
    }
};

}

#endif // ALIAS_H
