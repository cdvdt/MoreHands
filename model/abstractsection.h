/*
 *  abstractsection.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABSTRACTSECTION_H
#define ABSTRACTSECTION_H

#include <string>
#include "alias.h"
#include "abstractelement.h"

/**
 * Defines a base class from which Section, Music and Setlist are derived
 */
class AbstractSection
{
public:
    AbstractSection();

public:
    /**
     * Returns the name of this object
     */
    virtual std::string getName();
    /**
     * Sets the name for this object.
     * \param name name to be associated to this object
     */
    virtual void setName(std::string name);
    /**
     * \deprecated
     * Initiates this section, resets the current element to the first element.
     */
    virtual void startSection() = 0;
    /**
     * Associates an Alias to this object
     * \param alias alias for this object
     */
    virtual void setAlias(Alias alias);
    /**
     * Returns the Alias associated with this object
     * \return Alias associated with this object
     */
    virtual Alias getAlias();
    /**
     * Returns a string representation of this object
     * \return representation of this object.
     */
    virtual std::string getContent() = 0;
    /**
     * \deprecated
     * Returns the element pointed by this section. If currentElement is not valid (exceeds the number of elements held by this section) it returns a nullptr.
     * \return Element pointed by currentElement or nullpt if invalid
     */
    virtual AbstractElement* getCurrent() = 0;
    /**
     * \deprecated
     * Changes current element to next element to be used.
     * \return true if currentElement points to a valid element and false otherwise
     */
    virtual bool stepForward() = 0;
    /**
     * Amount of elements held by this object
     * \return Amount of elements held by this object
     */
    virtual int size() = 0;

protected:
    std::string name; ///< name associated with this object
    Alias alias; ///< Describes a shortcut to access a section

};

#endif // ABSTRACTSECTION_H
