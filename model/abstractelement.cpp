/*
 *  abstractelement.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abstractelement.h"

AbstractElement::AbstractElement()
{
}

std::string AbstractElement::getSurname() {
    return surname;
}

std::string AbstractElement::getName() {
    return name;
}

void AbstractElement::setName(std::string name) {
    this->name = name;
}

void AbstractElement::setSurname(std::string surname) {
    this->surname = surname;
}
