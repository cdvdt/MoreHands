/*
 *  music.h
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUSIC_H
#define MUSIC_H

#include "abstractsection.h"
#include "section.h"
#include <vector>

/**
 * Defines a Music by a sequence of sections.
 */
class Music: public AbstractSection {
public:
	Music();

	/**
	 * \deprecated
	 * Returns the currently pointed Section
	 * \return the currently pointed Section
	 */
	AbstractElement *getCurrent();
	/**
	 * \deprecated
	 * Updates the currently pointed section to the next Section
	 * \return true if the new Section is valid, and false otherwise
	 */
	bool stepForward();
	/**
	 * Amount of elements held by this object
	 * \return Amount of elements held by this object
	 */
	virtual int size();
	/**
	 * Returns an array of Section held by this object
	 * \return an array of Section held by this object
	 */
	Section** getSections();
	/**
	 * \deprecated
	 * Returns the number of sections inside this object
	 * \return the number of sections inside this object
	 */
	int getSectionsSize();
	/**
	 * Returns the section at index position
	 * \param index position of the object to be returned
	 * \return Section at position index
	 */
	Section *getSectionAt(int index);
	/**
	 * Returns the section at index position
	 * \param index position of the object to be returned
	 * \return Section at position index
	 */
	Section* operator[](int index);
	/**
	 * add a new Section to this Music. If an index is specified it inserts the new element at index position, else adds to the end of the list.
	 * \param newSection Section to be added
	 * \param index position for newSection to be added, if unspecified adds to the end of the list;
	 */
	void addSection(Section* newSection, int index = -1);
	/**
	 * Removes a Section indicated by its index from this Music.
	 * \param index position of the item to be removed
	 */
	void removeSection(int index);
private:
	std::vector<Section *> sections;
};

#endif // MUSIC_H
