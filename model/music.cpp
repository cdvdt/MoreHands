/*
 *  music.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "music.h"

Music::Music()
{

}

//TODO
Section** Music::getSections() {
    return sections.data();
}

Section* Music::operator [](int index) {
    return sections[index];
}

AbstractElement* Music::getCurrent() {
	return nullptr;
}

bool Music::stepForward() {
	return false;
}

int Music::size() {
	return sections.size();
}

int Music::getSectionsSize() {
	return size();
}

Section* Music::getSectionAt(int index) {
	return sections[index];
}

void Music::addSection(Section* newSection, int index) {
	if (index == -1) {
		sections.push_back(newSection);
	} else {
		sections.insert(sections.begin() + index, newSection);
	}
}

void Music::removeSection(int index) {
	sections.erase(sections.begin() + index);
}
