/*
 * InternalEvent.h
 *
 *  Created on: 12/10/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_INTERNAL_INTERNALEVENT_H_
#define MODEL_EVENTS_INTERNAL_INTERNALEVENT_H_

#include "../MoreHandsEvent.h"
#include <functional>

/**
 * @brief InternalEvent describing an internal event, it is an abstract event
 */
class InternalEvent: public MoreHandsEvent {
public:
	InternalEvent();
	virtual ~InternalEvent();

	virtual bool compareTo(const MoreHandsEvent *other) const;
	virtual bool compareTo(const DefaultMidiEvent *other) const;
	virtual bool compareTo(const SystemSideEvent *other) const;
	virtual bool compareTo(const InternalEvent *other) const = 0;

	virtual std::size_t hash() const;
};

#endif /* MODEL_EVENTS_INTERNAL_INTERNALEVENT_H_ */
