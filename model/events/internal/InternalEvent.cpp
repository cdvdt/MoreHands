/*
 * InternalEvent.cpp
 *
 *  Created on: 12/10/2017
 *      Author: cdvdt
 */

#include "InternalEvent.h"

InternalEvent::InternalEvent() {
}

InternalEvent::~InternalEvent() {
}

bool InternalEvent::compareTo(const MoreHandsEvent* other) const {
	return other->compareTo(this);
}

bool InternalEvent::compareTo(const DefaultMidiEvent* other) const {
	return false;
}

bool InternalEvent::compareTo(const SystemSideEvent* other) const {
	return false;
}

std::size_t InternalEvent::hash() const {
	return 0;
}
