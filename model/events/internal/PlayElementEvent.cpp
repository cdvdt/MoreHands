/*
 * PlayElementEvent.cpp
 *
 *  Created on: 12/10/2017
 *      Author: cdvdt
 */

#include "PlayElementEvent.h"

PlayElementEvent::PlayElementEvent() {
	// TODO Auto-generated constructor stub

}

PlayElementEvent::~PlayElementEvent() {
	// TODO Auto-generated destructor stub
}

bool PlayElementEvent::compareTo(const DefaultMidiEvent* other) const {
	return false;
}

bool PlayElementEvent::compareTo(const PlayElementEvent* other) const {
	return true;
}

bool PlayElementEvent::compareTo(const SystemSideEvent* other) const {
	return false;
}

bool PlayElementEvent::compareTo(const InternalEvent* other) const {
	return other->compareTo(this);
}
