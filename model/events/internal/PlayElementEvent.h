/*
 * PlayElementEvent.h
 *
 *  Created on: 12/10/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_INTERNAL_PLAYELEMENTEVENT_H_
#define MODEL_EVENTS_INTERNAL_PLAYELEMENTEVENT_H_

#include "InternalEvent.h"

class PlayElementEvent: public InternalEvent {
public:
	PlayElementEvent();
	virtual ~PlayElementEvent();

	virtual bool compareTo(const DefaultMidiEvent *other) const;
	virtual bool compareTo(const PlayElementEvent *other) const;
	virtual bool compareTo(const SystemSideEvent *other) const;
	virtual bool compareTo(const InternalEvent *other) const;
};

#endif /* MODEL_EVENTS_INTERNAL_PLAYELEMENTEVENT_H_ */
