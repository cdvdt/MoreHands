/*
 * MoreHandsEvent.h
 *
 *  Created on: 22/07/2017
 *      Author: cdvdt
 */

#ifndef MOREHANDSEVENT_H_
#define MOREHANDSEVENT_H_

#include <functional>
class SystemSideEvent;
class DefaultMidiEvent;
class InternalEvent;

/**
 * @brief Abstract class that defines all events inside MoreHands.
 */
class MoreHandsEvent {
public:
	MoreHandsEvent();
	virtual ~MoreHandsEvent();

	/**
	 * @brief Compares two MoreHandsEvent events
	 * @param other any subclass of MoreHandsEvent to be compared with
	 * @returns true if internally the same, false other wise
	 */
	bool operator == (MoreHandsEvent const& other) const;
	virtual bool compareTo(const MoreHandsEvent *other) const = 0;
	virtual bool compareTo(const SystemSideEvent *other) const = 0;
	virtual bool compareTo(const DefaultMidiEvent *other) const = 0;
	virtual bool compareTo(const InternalEvent *other) const = 0;

	virtual std::size_t hash() const = 0;
};

/*
 * Definition of hash for Alias. Not implemented yet
 */
namespace std {

template <>
struct hash<MoreHandsEvent>
{
    std::size_t operator()(const MoreHandsEvent& k) const
    {
        return k.hash();
    }
};

}

#endif /* MOREHANDSEVENT_H_ */
