/*
 * KeyboardEvent.h
 *
 *  Created on: 22/07/2017
 *      Author: cdvdt
 */

#ifndef KEYBOARDEVENT_H_
#define KEYBOARDEVENT_H_

#include <functional>
#include "SystemSideEvent.h"

/**
 * @brief SystemSideEvent describing a keyboard event
 *
 * Event thrown by the UI when a key is pressed, it has two fields, a key, representing the pressed key, and flags, representing modifiers held when key was pressed.
 */
class KeyboardEvent: public SystemSideEvent {
public:
	/**
	 * @brief Possible flags. All flags can be used together.
	 */
	enum EFlags {
		NONE = 0, ///< Used when no modifier is pressed (0x00)
		CTRL = 1, ///< Used when CTRL modifier is pressed (0x01)
		ALT = 1 << 1, ///< Used when ALT modifier is pressed (0x02)
		SHIFT = 1 << 2, ///< Used when SHIFT modifier is pressed (0x04)
		META = 1 << 3 ///< Used when META modifier is pressed (0x08)
	};
	/**
	 * @brief Constructs a new KeyboardEvent with the given parameters
	 * @param key pressed key
	 * @param flags Modifiers down when key was pressed
	 */
	KeyboardEvent(char key, EFlags flags);
	/**
	 * @brief Returns the modifiers down when key was pressed
	 * @return Modifiers down when key was pressed
	 */
	EFlags getFlags() const;
	/**
	 * @brief Returns the key that was pressed
	 * @return pressed key
	 */
	char getKey() const;

	/**
	 * @name compareTo
	 * @brief Compares this event to another MoreHandsEvent event.
	 * @param other Event to be compared to.
	 * @return True if same class, key and modifiers, false otherwise.
	 */
	//@{
	/** Compares this event to another MoreHandsEvent event. */
	virtual bool compareTo(const DefaultMidiEvent *other) const;
	virtual bool compareTo(const KeyboardEvent *other) const;
	virtual bool compareTo(const MoreHandsEvent *other) const;
	virtual bool compareTo(const SystemSideEvent *other) const;
	virtual bool compareTo(const InternalEvent *other) const;
	//@}

	/**
	 * @brief
	 */
	virtual std::size_t hash() const;
private:
	char key;
	EFlags flags;
};

#endif /* KEYBOARDEVENT_H_ */
