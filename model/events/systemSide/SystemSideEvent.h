/*
 * SystemSideEvent.h
 *
 *  Created on: 22/07/2017
 *      Author: cdvdt
 */

#ifndef SYSTEMSIDEEVENT_H_
#define SYSTEMSIDEEVENT_H_

#include "../MoreHandsEvent.h"
#include "../midiSide/DefaultMidiEvent.h"
#include <functional>

class KeyboardEvent;

/**
 * @brief MoreHandsEvent describing an event from system side, it is an abstract event
 *
 * Event thrown by the UI when a key is pressed, it has two fields, a key, representing the pressed key, and flags, representing modifiers held when key was pressed.
 */
class SystemSideEvent : public MoreHandsEvent
{
public:
	SystemSideEvent();
	virtual ~SystemSideEvent();

	virtual bool compareTo(const MoreHandsEvent *other) const;
	virtual bool compareTo(const DefaultMidiEvent *other) const;
	virtual bool compareTo(const SystemSideEvent *other) const = 0;
	virtual bool compareTo(const KeyboardEvent *other) const = 0;
	virtual bool compareTo(const InternalEvent *other) const;
};

#endif /* SYSTEMSIDEEVENT_H_ */
