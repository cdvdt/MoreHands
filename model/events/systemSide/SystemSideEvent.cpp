/*
 * SystemSideEvent.cpp
 *
 *  Created on: 22/07/2017
 *      Author: cdvdt
 */

#include "SystemSideEvent.h"

SystemSideEvent::SystemSideEvent() {
	// TODO Auto-generated constructor stub

}

SystemSideEvent::~SystemSideEvent() {
}

bool SystemSideEvent::compareTo(const MoreHandsEvent* other) const {
	return other->compareTo(this);
}

bool SystemSideEvent::compareTo(const DefaultMidiEvent* other) const {
	return false;
}

bool SystemSideEvent::compareTo(const InternalEvent* other) const {
	return false;
}
/*bool SystemSideEvent::operator ==(const MoreHandsEvent& other) const {
	//return (*this) == other;
	return other.operator ==(*this);
}

bool SystemSideEvent::operator ==(const SystemSideEvent& other) const {
	return other == (*this);
}

bool SystemSideEvent::operator ==(const DefaultMidiEvent& other) const {
	return false;
}*/


