/*
 * KeyboardEvent.cpp
 *
 *  Created on: 22/07/2017
 *      Author: cdvdt
 */

#include "KeyboardEvent.h"
#include <iostream>

KeyboardEvent::KeyboardEvent(char key, EFlags flags) : key(key), flags(flags){
}

KeyboardEvent::EFlags KeyboardEvent::getFlags() const {
	return flags;
}

char KeyboardEvent::getKey() const {
	return key;
}

bool KeyboardEvent::compareTo(const MoreHandsEvent* other) const {
	return other->compareTo(((const KeyboardEvent *) this));
}

bool KeyboardEvent::compareTo(const DefaultMidiEvent* other) const {
	return false;
}

bool KeyboardEvent::compareTo(const KeyboardEvent* other) const {
	return ((this->key == other->key) && (this->flags == other->flags));
}

bool KeyboardEvent::compareTo(const SystemSideEvent* other) const {
	return other->compareTo(this);
}

bool KeyboardEvent::compareTo(const InternalEvent* other) const {
	return false;
}

std::size_t KeyboardEvent::hash() const {
	return 0;
}

