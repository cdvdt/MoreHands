/*
 * ControlMidiEvent.cpp
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#include "ControlMidiEvent.h"

ControlMidiEvent::ControlMidiEvent() {
	// TODO Auto-generated constructor stub

}

ControlMidiEvent::ControlMidiEvent(int type, unsigned char flags,
		unsigned char tag, unsigned char queue, unsigned char channel,
		unsigned int param, signed int value, EventAdress source,
		EventAdress dest) :
		DefaultMidiEvent(type, flags, tag, queue, source, dest), channel(
				channel), param(param), value(value) {
}

ControlMidiEvent::~ControlMidiEvent() {
	// TODO Auto-generated destructor stub
}

std::string ControlMidiEvent::to_string() {
	return DefaultMidiEvent::to_string() + "\nChannel: "
			+ std::to_string(channel) + "\nParam: "
			+ std::to_string(param) + "\nValue: " + std::to_string(value)
					+ "\n";
}
