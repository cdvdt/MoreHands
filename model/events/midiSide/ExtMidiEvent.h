/*
 * ExtMidiEvent.h
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_EXTMIDIEVENT_H_
#define MODEL_EVENTS_EXTMIDIEVENT_H_

#include "DefaultMidiEvent.h"

class ExtMidiEvent: public DefaultMidiEvent {
public:
	ExtMidiEvent();
	virtual ~ExtMidiEvent();
};

#endif /* MODEL_EVENTS_EXTMIDIEVENT_H_ */
