/*
 * NoteMidiEvent.cpp
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#include "NoteMidiEvent.h"

std::string NoteMidiEvent::to_string() {
	return DefaultMidiEvent::to_string() + "\nChannel: "
			+ std::to_string(channel) + "\nNote: " + std::to_string(note)
			+ "\nVelocity: " + std::to_string(velocity) + "\nOff velocity: "
			+ std::to_string(off_velocity) + "\nType: " + typeAsString() + "\n";
}

NoteMidiEvent::~NoteMidiEvent() {
}

NoteMidiEvent::NoteMidiEvent(int type, unsigned char flags, unsigned char tag,
		unsigned char queue, unsigned char channel, unsigned char note,
		unsigned char velocity, unsigned char off_velocity,
		unsigned int duration, NoteType noteType, EventAdress source,
		EventAdress dest) :
		DefaultMidiEvent(type, flags, tag, queue, source, dest), channel(
				channel), note(note), velocity(velocity), duration(duration), noteType(
				noteType) {
}

NoteMidiEvent::NoteMidiEvent() {
}

std::string NoteMidiEvent::typeAsString() {
	switch (noteType) {
	case NOTE:
		return "NOTE";
	case NOTE_ON:
		return "NOTE_ON";
	case NOTE_OFF:
		return "NOTE_OFF";
	case KEY_PRESSURE:
		return "KEY_PRESSURE";
	default:
		return "";
	}
}
