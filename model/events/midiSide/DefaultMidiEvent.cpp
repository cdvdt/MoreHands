/*
 * AbstracMidiEvent.cpp
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#include "DefaultMidiEvent.h"
#include <iostream>

DefaultMidiEvent::DefaultMidiEvent() {
	// TODO Auto-generated constructor stub

}

DefaultMidiEvent::~DefaultMidiEvent() {
	// TODO Auto-generated destructor stub
}

DefaultMidiEvent::DefaultMidiEvent(int type, unsigned char flags,
		unsigned char tag, unsigned char queue, EventAdress source,
		EventAdress dest) :
		type(type), flags(flags), tag(tag), queue(queue), source(source), dest(
				dest) {
}

std::string DefaultMidiEvent::to_string() {
	return "type: " + std::to_string(type) + "\nFlags: " + std::to_string(flags)
			+ "\nTag: " + std::to_string(tag) + "\nQueue: "
			+ std::to_string(queue) + "\nSource: " + source.to_string()
			+ "\nDest: " + dest.to_string();
}

std::string EventAdress::to_string() {
	return std::to_string(client) + ":" + std::to_string(port);
}

bool DefaultMidiEvent::compareTo(const MoreHandsEvent* other) const {
	std::cout << "DM" << std::endl;
	return other->compareTo((const DefaultMidiEvent *)this);
}

bool DefaultMidiEvent::compareTo(const DefaultMidiEvent* other) const {
	std::cout << "DD" << std::endl;
	return false;
}

bool DefaultMidiEvent::compareTo(const SystemSideEvent* other) const {
	std::cout << "DS" << std::endl;
	return false;
}

bool DefaultMidiEvent::compareTo(const InternalEvent* other) const {
	return false;
}

std::size_t DefaultMidiEvent::hash() const {
	return 0;
}

