/*
 * ControlMidiEvent.h
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_CONTROLMIDIEVENT_H_
#define MODEL_EVENTS_CONTROLMIDIEVENT_H_

#include "DefaultMidiEvent.h"

class ControlMidiEvent: public DefaultMidiEvent {
public:
	ControlMidiEvent();
	ControlMidiEvent(int type, unsigned char flags,
			unsigned char tag, unsigned char queue, unsigned char channel,
			unsigned int param, signed int value, EventAdress source,
			EventAdress dest);
	virtual ~ControlMidiEvent();

	virtual std::string to_string();

	unsigned char channel;
	unsigned int param;
	signed int value;
};

#endif /* MODEL_EVENTS_CONTROLMIDIEVENT_H_ */
