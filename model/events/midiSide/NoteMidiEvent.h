/*
 * NoteMidiEvent.h
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_NOTEMIDIEVENT_H_
#define MODEL_EVENTS_NOTEMIDIEVENT_H_

#include "DefaultMidiEvent.h"

class NoteMidiEvent: public DefaultMidiEvent {
public:
	enum NoteType {
		NOTE, NOTE_ON, NOTE_OFF, KEY_PRESSURE
	};

public:
	NoteMidiEvent(int type, unsigned char flags, unsigned char tag,
			unsigned char queue, unsigned char channel, unsigned char note,
			unsigned char velocity, unsigned char off_velocity,
			unsigned int duration, NoteType noteType, EventAdress source,
			EventAdress dest);
	NoteMidiEvent();
	virtual ~NoteMidiEvent();
	virtual std::string to_string();

	std::string typeAsString();

	void setNoteType(NoteType noteType) {
		this->noteType = noteType;
	}

private:
	unsigned char channel;
	unsigned char note;
	unsigned char velocity;
	unsigned char off_velocity;
	unsigned int duration;
	NoteType noteType;
};

#endif /* MODEL_EVENTS_NOTEMIDIEVENT_H_ */
