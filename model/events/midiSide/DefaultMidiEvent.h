/*
 * AbstracMidiEvent.h
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_DEFAULTMIDIEVENT_H_
#define MODEL_EVENTS_DEFAULTMIDIEVENT_H_

#include <string>
#include "../MoreHandsEvent.h"
#include <functional>

class SystemSideEvent;

struct EventAdress {
	unsigned char client;
	unsigned char port;

	std::string to_string();
};

class DefaultMidiEvent : public MoreHandsEvent{
public:
	DefaultMidiEvent();
	DefaultMidiEvent(int type, unsigned char flags, unsigned char tag,
			unsigned char queue, EventAdress source, EventAdress dest);
	virtual ~DefaultMidiEvent();

	virtual std::string to_string();

	virtual bool compareTo(const DefaultMidiEvent *other) const;
	virtual bool compareTo(const SystemSideEvent *other) const;
	virtual bool compareTo(const MoreHandsEvent *other) const;
	virtual bool compareTo(const InternalEvent *other) const;

	virtual std::size_t hash() const;

	int type;

	unsigned char flags;
	unsigned char tag;
	unsigned char queue;

	EventAdress source;
	EventAdress dest;
};

#endif /* MODEL_EVENTS_DEFAULTMIDIEVENT_H_ */
