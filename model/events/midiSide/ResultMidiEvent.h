/*
 * ResultMidiEvent.h
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_RESULTMIDIEVENT_H_
#define MODEL_EVENTS_RESULTMIDIEVENT_H_

#include "DefaultMidiEvent.h"

class ResultMidiEvent: public DefaultMidiEvent {
public:
	ResultMidiEvent();
	virtual ~ResultMidiEvent();
};

#endif /* MODEL_EVENTS_RESULTMIDIEVENT_H_ */
