/*
 * ConnectMidiEvent.h
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_CONNECTMIDIEVENT_H_
#define MODEL_EVENTS_CONNECTMIDIEVENT_H_

#include "DefaultMidiEvent.h"

class ConnectMidiEvent: public DefaultMidiEvent {
public:
	ConnectMidiEvent();
	virtual ~ConnectMidiEvent();
};

#endif /* MODEL_EVENTS_CONNECTMIDIEVENT_H_ */
