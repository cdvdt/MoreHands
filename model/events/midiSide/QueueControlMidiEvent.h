/*
 * QueueControlMidiEvent.h
 *
 *  Created on: 01/07/2017
 *      Author: cdvdt
 */

#ifndef MODEL_EVENTS_QUEUECONTROLMIDIEVENT_H_
#define MODEL_EVENTS_QUEUECONTROLMIDIEVENT_H_

#include "DefaultMidiEvent.h"

class QueueControlMidiEvent: public DefaultMidiEvent {
public:
	QueueControlMidiEvent();
	virtual ~QueueControlMidiEvent();
};

#endif /* MODEL_EVENTS_QUEUECONTROLMIDIEVENT_H_ */
