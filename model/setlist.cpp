/*
 *  setlist.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "setlist.h"

Setlist::Setlist()
{

}

//TODO
Music* *Setlist::getSetlist(){
    return setlist.data();
}

//TODO
int Setlist::size(){
    return setlist.size();
}

//TODO
Music* Setlist::operator[](int index) {
    return setlist[index];
}

Music* Setlist::getCurrent() {
	return nullptr;
}

bool Setlist::stepForward() {
	return false;
}

void Setlist::updateCurrent(Music* music, Section* Section) {
}

int Setlist::getSetlistSize() {
	return size();
}

void Setlist::addMusic(Music* newMusic, int index) {
	if (index == -1) {
		setlist.push_back(newMusic);
	} else {
		setlist.insert(setlist.begin() + index, newMusic);
	}
}

void Setlist::removeMusic(int index) {
	setlist.erase(setlist.begin() + index);
}

std::string Setlist::getContent() {
	std::string result;
	for(Music* current : setlist) {
		result += current->getContent() + '\n';
	}

	return result;
}
