/*
 *  section.cpp
 *  This file is part of MoreHands.
 *
 *  Copyright (C) 2017 - Sergio Rodrigues de Oliveira Filho
 *
 *  MoreHands is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MoreHands is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MoreHands.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "section.h"

Section::Section()
{

}

void Section::startSection() {
    currentElement = 0;
}

bool Section::stepForward() {
    currentElement++;
    return currentElement < (int) elements.size();
}

AbstractElement* Section::getCurrent() {
    if (currentElement < (int) elements.size()) return elements[currentElement];
    else return nullptr;
}

void Section::addElement(AbstractElement* element) {
    elements.push_back(element);
}
void Section::addElement(AbstractElement* element, int index) {
    elements.insert(elements.begin() + index, element);
}

std::string contentAsString(std::vector<AbstractElement *> elements) {
    std::string result;
    for(int i = 0; i < (int) elements.size(); i++) {
        result += elements[i]->getName() + ":\t" + elements[i]->getContent() + "\t(" + elements[i]->getSurname() + ")";
        if (i < (int) elements.size() - 1)
            result += "\n";
    }
    return result;
}

std::string Section::getContent() {
    return contentAsString(elements);
}

AbstractElement* Section::operator [](int index) {
    return elements[index];
}

int Section::size() {
    return elements.size();
}
